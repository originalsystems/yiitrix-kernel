<?php

declare(strict_types=1);

return [
    \yii\bootstrap\BootstrapPluginAsset::class => __DIR__ . '/yii/bootstrap/BootstrapPluginAsset.php',
    \yii\helpers\Html::class                   => __DIR__ . '/yii/helpers/Html.php',
];
