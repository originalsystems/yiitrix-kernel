<?php

declare(strict_types=1);

namespace yiitrix\console;

class Application extends \yii\console\Application
{
    public $languages = [];
}
