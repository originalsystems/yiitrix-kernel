<?php

declare(strict_types=1);

namespace yiitrix\console\controllers;

use yii\db\Query;
use yii2kernel\console\Controller;
use yiitrix\models\File;

class UploadController extends Controller
{
    public function actionDeleteUnused()
    {
        $sql = <<<SQL
SELECT 
    "table_schema",
    "table_name", 
    "column_name", 
    "foreign_column_name"
FROM "foreign_key_relation"
WHERE "foreign_table_schema" = 'public' AND "foreign_table_name" = 'file'
SQL;

        $rows = \Yii::$app->getDb()->createCommand($sql)->queryAll();

        if (count($rows) > 0) {
            $query = new Query();

            $query->select('"file"."id"');
            $query->from('"public"."file"');

            $i = 0;

            foreach ($rows as $row) {
                $alias = 'ftable_' . ($i++);

                $sc   = $row['table_schema'];
                $tb   = $row['table_name'];
                $col  = $row['column_name'];
                $fCol = $row['foreign_column_name'];

                $query->leftJoin("\"{$sc}\".\"{$tb}\" AS \"{$alias}\"", "\"{$alias}\".\"{$col}\" = \"file\".\"{$fCol}\"");
                $query->andWhere("\"{$alias}\".\"{$col}\" IS NULL");
            }

            $list = $query->createCommand()->queryColumn();

            if (count($list) > 0) {
                File::deleteAll(['id' => $list]);
            }
        }
    }

    public function actionClearDeleted()
    {
        $dir  = \Yii::getAlias('@upload');
        $rows = \Yii::$app->getDb()->createCommand('SELECT * FROM "public"."deleted_file"')->queryAll();

        foreach ($rows as $row) {
            $path = \Yii::getAlias($row['path'], false);

            if (is_string($path) && file_exists($path)) {
                unlink($path);
            }
        }

        \Yii::$app->getDb()->createCommand('DELETE FROM "public"."deleted_file"')->query();

        passthru("find {$dir} -type d -empty -delete & 1>/dev/null 2>&1");
    }
}
