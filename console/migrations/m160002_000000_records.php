<?php

declare(strict_types=1);

use yii\db\Migration;

class m160002_000000_records extends Migration
{
    public function safeUp()
    {
        $sql = <<<SQL
CREATE SCHEMA "record";
SQL;
        $this->execute($sql);

        $sql = <<<SQL
CREATE TABLE "record"."block" (
    "code"       VARCHAR(64)  NOT NULL,
    "is_catalog" BOOLEAN      NOT NULL,
    "name"       VARCHAR(255) NOT NULL,
    "fields"     JSONB        NOT NULL DEFAULT '[]',
    "sort"       INT2         NOT NULL,
    "created_at" TIMESTAMPTZ  NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMPTZ  NOT NULL,
    PRIMARY KEY ("code")
);
SQL;
        $this->execute($sql);

        $sql = <<<SQL
CREATE TABLE "record"."section" (
    "code"         VARCHAR(64)  NOT NULL,
    "block_code"   VARCHAR(64)  NOT NULL,
    "section_code" VARCHAR(64)  NULL,
    "is_active"    BOOLEAN      NOT NULL,
    "name"         VARCHAR(255) NOT NULL,
    "fields"       JSONB        NOT NULL DEFAULT '[]',
    "sort"         INT2         NOT NULL,
    "created_at"   TIMESTAMPTZ  NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at"   TIMESTAMPTZ  NOT NULL,
    PRIMARY KEY ("code", "block_code"),
    FOREIGN KEY ("block_code")                 REFERENCES "record"."block"   ("code")               ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY ("section_code", "block_code") REFERENCES "record"."section" ("code", "block_code") ON UPDATE CASCADE ON DELETE CASCADE
);
SQL;
        $this->execute($sql);

        $sql = <<<SQL
CREATE TABLE "record"."element" (
    "code"         VARCHAR(64)  NOT NULL,
    "block_code"   VARCHAR(64)  NOT NULL,
    "section_code" VARCHAR(64)  NOT NULL,
    "is_active"    BOOLEAN      NOT NULL,
    "name"         VARCHAR(255) NOT NULL,
    "fields"       JSONB        NOT NULL DEFAULT '[]',
    "sort"         INT2         NOT NULL,
    "created_at"   TIMESTAMPTZ  NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at"   TIMESTAMPTZ  NOT NULL,
    PRIMARY KEY ("code", "block_code"),
    FOREIGN KEY ("block_code")                 REFERENCES "record"."block"   ("code")               ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY ("section_code", "block_code") REFERENCES "record"."section" ("code", "block_code") ON UPDATE CASCADE ON DELETE CASCADE
);
SQL;
        $this->execute($sql);

        $sql = <<<SQL
CREATE VIEW "record"."section_tree" AS (
    WITH RECURSIVE "section_tree" AS (
        SELECT
            "record"."section"."code"         AS "child_code",
            "record"."section"."block_code"   AS "block_code",
            "record"."section"."section_code" AS "section_code",
            1                                 AS "depth"
        FROM "record"."section"

        UNION ALL

        SELECT
            "section_tree"."child_code" AS "child_code",
            "section_tree"."block_code" AS "block_code",
            "parent"."section_code"     AS "section_code",
            "depth" + 1                 AS "depth"
        FROM "section_tree"
            INNER JOIN "record"."section" AS "parent" ON "parent"."code" = "section_tree"."section_code"
    )
    SELECT
        "section_tree"."child_code"   AS "child_code",
        "section_tree"."block_code"   AS "block_code",
        "section_tree"."section_code" AS "section_code",
        "section_tree"."depth"        AS "depth"
    FROM "section_tree"
);
SQL;
        $this->execute($sql);

        $this->execute('CREATE INDEX "block_idx_is_catalog" ON "record"."block" USING BTREE ("is_catalog")');
        $this->execute('CREATE INDEX "block_idx_name"       ON "record"."block" USING BTREE ("name")');
        $this->execute('CREATE INDEX "block_idx_sort"       ON "record"."block" USING BTREE ("sort")');
        $this->execute('CREATE INDEX "block_idx_fields"     ON "record"."block" USING GIN   ("fields")');

        $this->execute('CREATE INDEX "section_idx_section_code_block_code" ON "record"."section" USING BTREE ("section_code", "block_code")');
        $this->execute('CREATE INDEX "section_idx_is_active"               ON "record"."section" USING BTREE ("is_active")');
        $this->execute('CREATE INDEX "section_idx_name"                    ON "record"."section" USING BTREE ("name")');
        $this->execute('CREATE INDEX "section_idx_fields"                  ON "record"."section" USING GIN   ("fields")');
        $this->execute('CREATE INDEX "section_idx_sort"                    ON "record"."section" USING BTREE ("sort")');
        $this->execute('CREATE INDEX "section_idx_created_at"              ON "record"."section" USING BTREE ("created_at")');

        $this->execute('CREATE INDEX "element_idx_section_code_block_code" ON "record"."element" USING BTREE ("section_code", "block_code")');
        $this->execute('CREATE INDEX "element_idx_is_active"               ON "record"."element" USING BTREE ("is_active")');
        $this->execute('CREATE INDEX "element_idx_name"                    ON "record"."element" USING BTREE ("name")');
        $this->execute('CREATE INDEX "element_idx_fields"                  ON "record"."element" USING GIN   ("fields")');
        $this->execute('CREATE INDEX "element_idx_sort"                    ON "record"."element" USING BTREE ("sort")');
        $this->execute('CREATE INDEX "element_idx_created_at"              ON "record"."element" USING BTREE ("created_at")');
    }

    public function safeDown()
    {
        $this->execute('DROP SCHEMA "record" CASCADE');
    }
}
