<?php

declare(strict_types=1);

use yii\db\Migration;

class m160003_000000_catalog extends Migration
{
    public function safeUp()
    {
        $sql = <<<SQL
CREATE SCHEMA "catalog";
SQL;
        $this->execute($sql);

        $sql = <<<SQL
CREATE TABLE "catalog"."currency" (
    "code"       VARCHAR(16)  NOT NULL,
    "name"       VARCHAR(128) NOT NULL UNIQUE,
    "fields"     JSONB        NOT NULL DEFAULT '[]',
    "created_at" TIMESTAMPTZ  DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY ("code")
);
SQL;
        $this->execute($sql);

        $sql = <<<SQL
CREATE TABLE "catalog"."price" (
    "code"          VARCHAR(64)  NOT NULL,
    "currency_code" VARCHAR(16)  NOT NULL,
    "name"          VARCHAR(255) NOT NULL,
    "fields"        JSONB        NOT NULL DEFAULT '[]',
    "created_at"    TIMESTAMPTZ  NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY ("code"),
    FOREIGN KEY ("currency_code") REFERENCES "catalog"."currency" ("code") ON UPDATE CASCADE ON DELETE CASCADE
);
SQL;
        $this->execute($sql);

        $sql = <<<SQL
CREATE TABLE "catalog"."element_price" (
    "element_code" VARCHAR(64)    NOT NULL,
    "block_code"   VARCHAR(64)    NOT NULL,
    "price_code"   VARCHAR(64)    NOT NULL,
    "value"        NUMERIC(24, 8) NOT NULL CHECK ("value" >= 0),
    "created_at"   TIMESTAMPTZ    NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at"   TIMESTAMPTZ    NOT NULL,
    PRIMARY KEY ("element_code", "block_code", "price_code"),
    FOREIGN KEY ("element_code", "block_code") REFERENCES "record"."element" ("code", "block_code") ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY ("block_code")                 REFERENCES "record"."block"   ("code")               ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY ("price_code")                 REFERENCES "catalog"."price"  ("code")               ON UPDATE CASCADE ON DELETE CASCADE
);
SQL;
        $this->execute($sql);

        $this->execute('CREATE INDEX "currency_idx_name"            ON "catalog"."currency"      USING BTREE ("name")');
        $this->execute('CREATE INDEX "currency_idx_fields"          ON "catalog"."currency"      USING GIN   ("fields")');
        $this->execute('CREATE INDEX "price_idx_currency_code"      ON "catalog"."price"         USING BTREE ("currency_code")');
        $this->execute('CREATE INDEX "price_idx_name"               ON "catalog"."price"         USING BTREE ("name")');
        $this->execute('CREATE INDEX "price_idx_fields"             ON "catalog"."price"         USING GIN   ("fields")');
        $this->execute('CREATE INDEX "element_price_idx_value"      ON "catalog"."element_price" USING BTREE ("value")');
        $this->execute('CREATE INDEX "element_price_idx_created_at" ON "catalog"."element_price" USING BTREE ("created_at")');
    }

    public function safeDown()
    {
        $this->execute('DROP SCHEMA "catalog" CASCADE');
    }
}
