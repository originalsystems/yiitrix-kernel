<?php

declare(strict_types=1);

use yii\db\Migration;

class m160004_000000_sale extends Migration
{
    public function safeUp()
    {
        $sql = <<<SQL
CREATE SCHEMA "sale";
SQL;
        $this->execute($sql);

        $sql = <<<SQL
CREATE TABLE "sale"."status" (
    "code"        VARCHAR(64)  NOT NULL,
    "is_new_sale" BOOLEAN      NOT NULL,
    "name"        VARCHAR(255) NOT NULL,
    "sort"        INT2         NOT NULL,
    "created_at"  TIMESTAMPTZ  DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY ("code")
);
SQL;
        $this->execute($sql);

        $sql = <<<SQL
CREATE TABLE "sale"."sale" (
    "id"          SERIAL4     NOT NULL,
    "user_id"     INT4        NOT NULL,
    "status_code" VARCHAR(64) NOT NULL,
    "created_at"  TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at"  TIMESTAMPTZ NOT NULL,
    PRIMARY KEY ("id"),
    FOREIGN KEY ("user_id")     REFERENCES "public"."user" ("id")   ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY ("status_code") REFERENCES "sale"."status" ("code") ON UPDATE CASCADE ON DELETE CASCADE
);
SQL;
        $this->execute($sql);

        $sql = <<<SQL
CREATE TABLE "sale"."basket" (
    "id"           SERIAL4     NOT NULL,
    "sale_id"      INT4        NULL,
    "element_code" VARCHAR(64) NOT NULL,
    "block_code"   VARCHAR(64) NOT NULL,
    "quantity"     INT2        NOT NULL,
    "created_at"   TIMESTAMPTZ DEFAULT CURRENT_TIMESTAMP,
    "updated_at"   TIMESTAMPTZ,
    PRIMARY KEY ("id"),
    FOREIGN KEY ("sale_id")                    REFERENCES "sale"."sale"      ("id")                 ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY ("element_code", "block_code") REFERENCES "record"."element" ("code", "block_code") ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY ("block_code")                 REFERENCES "record"."block"   ("code")               ON UPDATE CASCADE ON DELETE CASCADE
);
SQL;
        $this->execute($sql);

        $this->execute('CREATE UNIQUE INDEX "status_idx_is_new_sale" ON "sale"."status" USING BTREE ("is_new_sale") WHERE "is_new_sale" = TRUE');
        $this->execute('CREATE INDEX        "status_idx_sort"        ON "sale"."status" USING BTREE ("sort")');

        $this->execute('CREATE INDEX "sale_idx_user_id"     ON "sale"."sale" USING BTREE ("user_id")');
        $this->execute('CREATE INDEX "sale_idx_status_code" ON "sale"."sale" USING BTREE ("status_code")');
        $this->execute('CREATE INDEX "sale_idx_created_at"  ON "sale"."sale" USING BTREE ("created_at")');

        $this->execute('CREATE INDEX "basket_idx_sale_id"                 ON "sale"."basket" USING BTREE ("sale_id")');
        $this->execute('CREATE INDEX "basket_idx_element_code_block_code" ON "sale"."basket" USING BTREE ("element_code", "block_code")');
        $this->execute('CREATE INDEX "basket_idx_quantity"                ON "sale"."basket" USING BTREE ("quantity")');
    }

    public function safeDown()
    {
        $this->execute('DROP SCHEMA "sale" CASCADE');
    }
}
