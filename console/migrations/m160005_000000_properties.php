<?php

declare(strict_types=1);

use yii\db\Migration;

class m160005_000000_properties extends Migration
{
    public function safeUp()
    {
        $sql = <<<SQL
CREATE SCHEMA "property";
SQL;
        $this->execute($sql);

        $sql = <<<SQL
CREATE TABLE "property"."property" (
    "code"        VARCHAR(64)  NOT NULL,
    "is_required" BOOLEAN      NOT NULL,
    "is_multiple" BOOLEAN      NOT NULL,
    "type"        VARCHAR(64)  NOT NULL,
    "name"        VARCHAR(255) NOT NULL,
    "sort"        INT2         NOT NULL,
    "created_at"  TIMESTAMPTZ  NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at"  TIMESTAMPTZ  NOT NULL
);
SQL;
        $this->execute($sql);

        $sql = <<<SQL
CREATE TABLE "property"."property_enum" (
    "code"          VARCHAR(64)  NOT NULL,
    "property_code" VARCHAR(64)  NOT NULL,
    "name"          VARCHAR(255) NOT NULL,
    "sort"          INT2         NOT NULL,
    "created_at"    TIMESTAMPTZ  NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at"    TIMESTAMPTZ  NOT NULL
);
SQL;
        $this->execute($sql);

        $sql = <<<SQL
CREATE TABLE "property"."property_value" (
    "property_code"     VARCHAR(64)    NOT NULL,
    "boolean_value"     BOOLEAN        NULL,
    "string_value"      TEXT           NULL,
    "numeric_value"     NUMERIC(24, 8) NULL,
    "user_link_id"      INT4           NULL,
    "file_link_id"      INT4           NULL,
    "sale_link_id"      INT4           NULL,
    "element_link_code" VARCHAR(64)    NULL,
    "section_link_code" VARCHAR(64)    NULL,
    "block_link_code"   VARCHAR(64)    NULL,
    "enum_link_code"    VARCHAR(64)    NULL,
    "created_at"        TIMESTAMPTZ    NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at"        TIMESTAMPTZ    NOT NULL
);
SQL;
        $this->execute($sql);

        // Element properties

        $sql = <<<SQL
CREATE TABLE "property"."element_property" (
    "block_code" VARCHAR(64) NOT NULL,
    
    PRIMARY KEY ("code", "block_code"),
    
    FOREIGN KEY ("block_code") 
        REFERENCES "record"."block" ("code") 
        ON UPDATE CASCADE ON DELETE CASCADE
) INHERITS ("property"."property");
SQL;
        $this->execute($sql);

        $sql = <<<SQL
CREATE TABLE "property"."element_property_enum" (
    "block_code" VARCHAR(64) NOT NULL,
    
    PRIMARY KEY ("code", "property_code", "block_code"),
    
    FOREIGN KEY ("property_code", "block_code") 
        REFERENCES "property"."element_property" ("code", "block_code") 
        ON UPDATE CASCADE ON DELETE CASCADE
) INHERITS ("property"."property_enum");
SQL;
        $this->execute($sql);

        $sql = <<<SQL
CREATE TABLE "property"."element_property_value" (
    "element_code"  VARCHAR(64) NOT NULL,
    "block_code"    VARCHAR(64) NOT NULL,
    
    PRIMARY KEY ("property_code", "element_code", "block_code"),

    FOREIGN KEY ("element_code", "block_code")
        REFERENCES "record"."element" ("code", "block_code")
        ON UPDATE CASCADE ON DELETE CASCADE,
    
    FOREIGN KEY ("property_code", "block_code")
        REFERENCES "property"."element_property" ("code", "block_code")
        ON UPDATE CASCADE ON DELETE CASCADE,
    
    FOREIGN KEY ("user_link_id")                                  
        REFERENCES "public"."user" ("id")                                  
        ON UPDATE CASCADE ON DELETE CASCADE,
        
    FOREIGN KEY ("file_link_id")                                  
        REFERENCES "public"."file" ("id")                                  
        ON UPDATE CASCADE ON DELETE CASCADE,
        
    FOREIGN KEY ("sale_link_id")                                  
        REFERENCES "sale"."sale" ("id")                                  
        ON UPDATE CASCADE ON DELETE CASCADE,
        
    FOREIGN KEY ("element_link_code", "block_link_code")          
        REFERENCES "record"."element" ("code", "block_code")                  
        ON UPDATE CASCADE ON DELETE CASCADE,
        
    FOREIGN KEY ("section_link_code", "block_link_code")          
        REFERENCES "record"."section" ("code", "block_code")                  
        ON UPDATE CASCADE ON DELETE CASCADE,
        
    FOREIGN KEY ("enum_link_code", "property_code", "block_code") 
        REFERENCES "property"."element_property_enum" ("code", "property_code", "block_code") 
        ON UPDATE CASCADE ON DELETE CASCADE
) INHERITS ("property"."property_value");
SQL;
        $this->execute($sql);

        $this->execute('CREATE INDEX "element_property_idx_is_required" ON "property"."element_property" USING BTREE ("is_required")');
        $this->execute('CREATE INDEX "element_property_idx_is_multiple" ON "property"."element_property" USING BTREE ("is_multiple")');
        $this->execute('CREATE INDEX "element_property_idx_type"        ON "property"."element_property" USING BTREE ("type")');
        $this->execute('CREATE INDEX "element_property_idx_name"        ON "property"."element_property" USING BTREE ("name")');
        $this->execute('CREATE INDEX "element_property_idx_sort"        ON "property"."element_property" USING BTREE ("sort")');

        $this->execute('CREATE INDEX "element_property_enum_idx_sort" ON "property"."element_property_enum" USING BTREE ("sort")');

        $this->execute('CREATE INDEX "element_property_value_idx_boolean_value"                           ON "property"."element_property_value" USING BTREE ("boolean_value") WHERE "boolean_value" IS NOT NULL');
        $this->execute('CREATE INDEX "element_property_value_idx_string_value"                            ON "property"."element_property_value" USING BTREE ("string_value")  WHERE "string_value"  IS NOT NULL');
        $this->execute('CREATE INDEX "element_property_value_idx_numeric_value"                           ON "property"."element_property_value" USING BTREE ("numeric_value") WHERE "numeric_value" IS NOT NULL');
        $this->execute('CREATE INDEX "element_property_value_idx_user_link_id"                            ON "property"."element_property_value" USING BTREE ("user_link_id")  WHERE "user_link_id"  IS NOT NULL');
        $this->execute('CREATE INDEX "element_property_value_idx_file_link_id"                            ON "property"."element_property_value" USING BTREE ("file_link_id")  WHERE "file_link_id"  IS NOT NULL');
        $this->execute('CREATE INDEX "element_property_value_idx_sale_link_id"                            ON "property"."element_property_value" USING BTREE ("sale_link_id")  WHERE "sale_link_id"  IS NOT NULL');
        $this->execute('CREATE INDEX "element_property_value_idx_element_link_code_block_link_code"       ON "property"."element_property_value" USING BTREE ("element_link_code", "block_link_code") WHERE "element_link_code" IS NOT NULL AND "block_link_code" IS NOT NULL');
        $this->execute('CREATE INDEX "element_property_value_idx_section_link_code_block_link_code"       ON "property"."element_property_value" USING BTREE ("section_link_code", "block_link_code") WHERE "section_link_code" IS NOT NULL AND "block_link_code" IS NOT NULL');
        $this->execute('CREATE INDEX "element_property_value_idx_enum_link_code_property_code_block_code" ON "property"."element_property_value" USING BTREE ("enum_link_code", "property_code", "block_code") WHERE "enum_link_code" IS NOT NULL');

        // User properties

        $sql = <<<SQL
CREATE TABLE "property"."user_property" (
    PRIMARY KEY ("code")
) INHERITS ("property"."property");
SQL;
        $this->execute($sql);

        $sql = <<<SQL
CREATE TABLE "property"."user_property_enum" (
    PRIMARY KEY ("code", "property_code"),
    
    FOREIGN KEY ("property_code") 
        REFERENCES "property"."user_property" ("code") 
        ON UPDATE CASCADE ON DELETE CASCADE
) INHERITS ("property"."property_enum");
SQL;
        $this->execute($sql);

        $sql = <<<SQL
CREATE TABLE "property"."user_property_value" (
    "user_id" INT4 NOT NULL,

    PRIMARY KEY ("property_code", "user_id"),

    FOREIGN KEY ("user_id")
        REFERENCES "public"."user" ("id")
        ON UPDATE CASCADE ON DELETE CASCADE,
    
    FOREIGN KEY ("property_code")
        REFERENCES "property"."user_property" ("code")
        ON UPDATE CASCADE ON DELETE CASCADE,
    
    FOREIGN KEY ("user_link_id")                                  
        REFERENCES "public"."user" ("id")                                  
        ON UPDATE CASCADE ON DELETE CASCADE,
        
    FOREIGN KEY ("file_link_id")                                  
        REFERENCES "public"."file" ("id")                                  
        ON UPDATE CASCADE ON DELETE CASCADE,
        
    FOREIGN KEY ("sale_link_id")                                  
        REFERENCES "sale"."sale" ("id")                                  
        ON UPDATE CASCADE ON DELETE CASCADE,
        
    FOREIGN KEY ("element_link_code", "block_link_code")          
        REFERENCES "record"."element" ("code", "block_code")                  
        ON UPDATE CASCADE ON DELETE CASCADE,
        
    FOREIGN KEY ("section_link_code", "block_link_code")          
        REFERENCES "record"."section" ("code", "block_code")                  
        ON UPDATE CASCADE ON DELETE CASCADE,
        
    FOREIGN KEY ("enum_link_code", "property_code") 
        REFERENCES "property"."user_property_enum" ("code", "property_code") 
        ON UPDATE CASCADE ON DELETE CASCADE
) INHERITS ("property"."property_value");
SQL;
        $this->execute($sql);
        
        $this->execute('CREATE INDEX "user_property_idx_is_required" ON "property"."user_property" USING BTREE ("is_required")');
        $this->execute('CREATE INDEX "user_property_idx_is_multiple" ON "property"."user_property" USING BTREE ("is_multiple")');
        $this->execute('CREATE INDEX "user_property_idx_type"        ON "property"."user_property" USING BTREE ("type")');
        $this->execute('CREATE INDEX "user_property_idx_name"        ON "property"."user_property" USING BTREE ("name")');
        $this->execute('CREATE INDEX "user_property_idx_sort"        ON "property"."user_property" USING BTREE ("sort")');

        $this->execute('CREATE INDEX "user_property_enum_idx_sort" ON "property"."user_property_enum" USING BTREE ("sort")');

        $this->execute('CREATE INDEX "user_property_value_idx_boolean_value"                     ON "property"."user_property_value" USING BTREE ("boolean_value") WHERE "boolean_value" IS NOT NULL');
        $this->execute('CREATE INDEX "user_property_value_idx_string_value"                      ON "property"."user_property_value" USING BTREE ("string_value")  WHERE "string_value"  IS NOT NULL');
        $this->execute('CREATE INDEX "user_property_value_idx_numeric_value"                     ON "property"."user_property_value" USING BTREE ("numeric_value") WHERE "numeric_value" IS NOT NULL');
        $this->execute('CREATE INDEX "user_property_value_idx_user_link_id"                      ON "property"."user_property_value" USING BTREE ("user_link_id")  WHERE "user_link_id"  IS NOT NULL');
        $this->execute('CREATE INDEX "user_property_value_idx_file_link_id"                      ON "property"."user_property_value" USING BTREE ("file_link_id")  WHERE "file_link_id"  IS NOT NULL');
        $this->execute('CREATE INDEX "user_property_value_idx_sale_link_id"                      ON "property"."user_property_value" USING BTREE ("sale_link_id")  WHERE "sale_link_id"  IS NOT NULL');
        $this->execute('CREATE INDEX "user_property_value_idx_element_link_code_block_link_code" ON "property"."user_property_value" USING BTREE ("element_link_code", "block_link_code") WHERE "element_link_code" IS NOT NULL AND "block_link_code" IS NOT NULL');
        $this->execute('CREATE INDEX "user_property_value_idx_section_link_code_block_link_code" ON "property"."user_property_value" USING BTREE ("section_link_code", "block_link_code") WHERE "section_link_code" IS NOT NULL AND "block_link_code" IS NOT NULL');
        $this->execute('CREATE INDEX "user_property_value_idx_enum_link_code_property_code"      ON "property"."user_property_value" USING BTREE ("enum_link_code", "property_code") WHERE "enum_link_code" IS NOT NULL');

        // Sale properties

        $sql = <<<SQL
CREATE TABLE "property"."sale_property" (
    PRIMARY KEY ("code")
) INHERITS ("property"."property");
SQL;
        $this->execute($sql);

        $sql = <<<SQL
CREATE TABLE "property"."sale_property_enum" (
    PRIMARY KEY ("code", "property_code"),
    
    FOREIGN KEY ("property_code") 
        REFERENCES "property"."sale_property" ("code") 
        ON UPDATE CASCADE ON DELETE CASCADE
) INHERITS ("property"."property_enum");
SQL;
        $this->execute($sql);

        $sql = <<<SQL
CREATE TABLE "property"."sale_property_value" (
    "sale_id" INT4 NOT NULL,

    PRIMARY KEY ("property_code", "sale_id"),

    FOREIGN KEY ("sale_id")
        REFERENCES "sale"."sale" ("id")
        ON UPDATE CASCADE ON DELETE CASCADE,
    
    FOREIGN KEY ("property_code")
        REFERENCES "property"."user_property" ("code")
        ON UPDATE CASCADE ON DELETE CASCADE,
    
    FOREIGN KEY ("user_link_id")                                  
        REFERENCES "public"."user" ("id")                                  
        ON UPDATE CASCADE ON DELETE CASCADE,
        
    FOREIGN KEY ("file_link_id")                                  
        REFERENCES "public"."file" ("id")                                  
        ON UPDATE CASCADE ON DELETE CASCADE,
        
    FOREIGN KEY ("sale_link_id")                                  
        REFERENCES "sale"."sale" ("id")                                  
        ON UPDATE CASCADE ON DELETE CASCADE,
        
    FOREIGN KEY ("element_link_code", "block_link_code")          
        REFERENCES "record"."element" ("code", "block_code")                  
        ON UPDATE CASCADE ON DELETE CASCADE,
        
    FOREIGN KEY ("section_link_code", "block_link_code")          
        REFERENCES "record"."section" ("code", "block_code")                  
        ON UPDATE CASCADE ON DELETE CASCADE,
        
    FOREIGN KEY ("enum_link_code", "property_code") 
        REFERENCES "property"."user_property_enum" ("code", "property_code") 
        ON UPDATE CASCADE ON DELETE CASCADE
) INHERITS ("property"."property_value");
SQL;
        $this->execute($sql);

        $this->execute('CREATE INDEX "sale_property_idx_is_required" ON "property"."sale_property" USING BTREE ("is_required")');
        $this->execute('CREATE INDEX "sale_property_idx_is_multiple" ON "property"."sale_property" USING BTREE ("is_multiple")');
        $this->execute('CREATE INDEX "sale_property_idx_type"        ON "property"."sale_property" USING BTREE ("type")');
        $this->execute('CREATE INDEX "sale_property_idx_name"        ON "property"."sale_property" USING BTREE ("name")');
        $this->execute('CREATE INDEX "sale_property_idx_sort"        ON "property"."sale_property" USING BTREE ("sort")');

        $this->execute('CREATE INDEX "sale_property_enum_idx_sort" ON "property"."sale_property_enum" USING BTREE ("sort")');

        $this->execute('CREATE INDEX "sale_property_value_idx_boolean_value"                     ON "property"."sale_property_value" USING BTREE ("boolean_value") WHERE "boolean_value" IS NOT NULL');
        $this->execute('CREATE INDEX "sale_property_value_idx_string_value"                      ON "property"."sale_property_value" USING BTREE ("string_value")  WHERE "string_value"  IS NOT NULL');
        $this->execute('CREATE INDEX "sale_property_value_idx_numeric_value"                     ON "property"."sale_property_value" USING BTREE ("numeric_value") WHERE "numeric_value" IS NOT NULL');
        $this->execute('CREATE INDEX "sale_property_value_idx_user_link_id"                      ON "property"."sale_property_value" USING BTREE ("user_link_id")  WHERE "user_link_id"  IS NOT NULL');
        $this->execute('CREATE INDEX "sale_property_value_idx_file_link_id"                      ON "property"."sale_property_value" USING BTREE ("file_link_id")  WHERE "file_link_id"  IS NOT NULL');
        $this->execute('CREATE INDEX "sale_property_value_idx_sale_link_id"                      ON "property"."sale_property_value" USING BTREE ("sale_link_id")  WHERE "sale_link_id"  IS NOT NULL');
        $this->execute('CREATE INDEX "sale_property_value_idx_element_link_code_block_link_code" ON "property"."sale_property_value" USING BTREE ("element_link_code", "block_link_code") WHERE "element_link_code" IS NOT NULL AND "block_link_code" IS NOT NULL');
        $this->execute('CREATE INDEX "sale_property_value_idx_section_link_code_block_link_code" ON "property"."sale_property_value" USING BTREE ("section_link_code", "block_link_code") WHERE "section_link_code" IS NOT NULL AND "block_link_code" IS NOT NULL');
        $this->execute('CREATE INDEX "sale_property_value_idx_enum_link_code_property_code"      ON "property"."sale_property_value" USING BTREE ("enum_link_code", "property_code") WHERE "enum_link_code" IS NOT NULL');
    }

    public function safeDown()
    {
        $this->execute('DROP SCHEMA "property" CASCADE');
    }
}
