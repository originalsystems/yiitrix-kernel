<?php

declare(strict_types=1);

use yiitrix\rbac\AuthManager;

class m170000_000000_auth_data extends \yii\db\Migration
{
    public function safeUp()
    {
        $auth = \Yii::$app->getAuthManager();

        $adminRole  = $auth->createRole(AuthManager::ROLE_ADMIN);
        $clientRole = $auth->createRole(AuthManager::ROLE_CLIENT);

        $enterFrontendPerm = $auth->createPermission(AuthManager::ENTER_FRONTEND);
        $enterBackendPerm  = $auth->createPermission(AuthManager::ENTER_BACKEND);

        $auth->add($adminRole);
        $auth->add($clientRole);
        $auth->add($enterFrontendPerm);
        $auth->add($enterBackendPerm);

        $auth->addChild($adminRole, $enterBackendPerm);
        $auth->addChild($clientRole, $enterFrontendPerm);
    }

    public function safeDown()
    {
        $auth = \Yii::$app->getAuthManager();

        $auth->remove($auth->getRole(AuthManager::ROLE_ADMIN));
        $auth->remove($auth->getRole(AuthManager::ROLE_CLIENT));

        $auth->remove($auth->getPermission(AuthManager::ENTER_FRONTEND));
        $auth->remove($auth->getPermission(AuthManager::ENTER_BACKEND));
    }
}
