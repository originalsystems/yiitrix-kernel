<?php

declare(strict_types=1);

namespace yiitrix\helpers;

class BaseHtml extends \yii2kernel\helpers\BaseHtml
{
    public const TRUE_VALUE  = 'yes';
    public const FALSE_VALUE = 'no';

    /**
     * Returns translated options for grid boolean filter or something else
     *
     * @return array
     */
    public static function boolLabels(): array
    {
        return [
            self::TRUE_VALUE  => \Yii::t('yii', 'Yes'),
            self::FALSE_VALUE => \Yii::t('yii', 'No'),
        ];
    }
}
