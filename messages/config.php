<?php

declare(strict_types=1);

return [
    'sourcePath'       => '@yiitrix',
    'messagePath'      => '@yiitrix/messages',
    'languages'        => [
        'en',
        'ru',
    ],
    'translator'       => 'Yii::t',
    'sort'             => true,
    'overwrite'        => true,
    'removeUnused'     => true,
    'markUnused'       => false,
    'except'           => [
        '.git',
        '.gitignore',
        '.gitkeep',
        '/node_modules',
        '/vendor',
        '/messages',
    ],
    'only'             => [
        '*.php',
    ],
    'format'           => 'php',
    'catalog'          => 'messages',
    'ignoreCategories' => [
        'yii',
    ],
    'phpFileHeader'    => '',
    'phpDocBlock'      => false,
];
