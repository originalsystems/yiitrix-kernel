<?php

declare(strict_types=1);

namespace yiitrix\models;

use yii\base\InvalidCallException;
use yii\helpers\FileHelper;
use yii\helpers\Url;
use yii\web\UploadedFile;
use yii2kernel\base\FileInterface;
use yii2kernel\behaviors\TimestampBehavior;
use yii2kernel\db\ActiveRecord;

/**
 * @property int    $id
 * @property string $content_type
 * @property string $original_name
 * @property string $path
 * @property string $uri
 * @property string $created_at
 *
 * @property string $thumbUrl
 * @property string $webUrl
 * @property string $realPath
 *
 * @see     File::__get
 * @property string $thumbUrl60
 * @property string $thumbUrl120
 * @property string $thumbUrl240
 * @property string $thumbUrl360
 */
class File extends ActiveRecord implements FileInterface
{
    public const MIME_JPEG = 'image/jpeg';
    public const MIME_GIF  = 'image/gif';
    public const MIME_PNG  = 'image/png';
    public const MIME_SVG  = 'image/svg+xml';

    public static $imageMimeTypes = [
        self::MIME_JPEG,
        self::MIME_GIF,
        self::MIME_PNG,
        self::MIME_SVG,
    ];

    public static $resizeImageMimeTypes = [
        self::MIME_JPEG,
        self::MIME_PNG,
    ];

    /**
     * Getters for thumb urls like:
     * - "thumbUrl140",      equals $file->getThumbUrl(140, 140)
     * - "thumbUrlW120",     equals $file->getThumbUrl(120, null)
     * - "thumbUrlH160",     equals $file->getThumbUrl(null, 160)
     * - "thumbUrlW120H160", equals $file->getThumbUrl(120, 160)
     *
     * @param string $name
     *
     * @return mixed|string
     * @throws \yii\base\InvalidParamException
     */
    public function __get($name)
    {
        if (preg_match('#^thumbUrl(?P<size>\d+)$#', $name, $matches)) {
            $size = (int)$matches['size'];

            return $this->getThumbUrl($size, $size);
        }

        if (preg_match('#^thumbUrl(?:W(?P<width>\d+))?(?:H(?P<height>\d+))?$#', $name, $matches)) {
            $width  = !empty($matches['width']) ? (int)$matches['width'] : null;
            $height = !empty($matches['height']) ? (int)$matches['height'] : null;

            return $this->getThumbUrl($width, $height);
        }

        return parent::__get($name);
    }

    public function getRealPath(): string
    {
        return \Yii::getAlias($this->path);
    }

    public function getWebUrl(bool $absolute = false): ?string
    {
        if ($this->uri === null) {
            return null;
        }

        return Url::to($this->uri, $absolute);
    }

    public function getThumbUrl(int $width = null, int $height = null, bool $absolute = false): ?string
    {
        if ($this->uri === null) {
            return null;
        }

        if (in_array($this->content_type, self::$resizeImageMimeTypes, true) === false) {
            return $this->getWebUrl($absolute);
        }

        $prefix = "/thumb/{$width}x{$height}";

        if ($width === null && $height === null) {
            $prefix = '/thumb/160x-';
        } elseif ($width !== null && $height === null) {
            $prefix = "/thumb/{$width}x-";
        } elseif ($width === null && $height !== null) {
            $prefix = "/thumb/-x{$height}";
        }

        return Url::to($prefix . $this->uri, $absolute);
    }

    public static function tableName(): string
    {
        return 'public.file';
    }

    public function behaviors(): array
    {
        return [
            TimestampBehavior::class,
        ];
    }

    public function rules(): array
    {
        return [
            [['content_type', 'original_name', 'path'], 'required'],
            [['original_name'], 'string', 'max' => 256],
            [['content_type'], 'string', 'max' => 128],
            [['path', 'uri'], 'string', 'max' => 512],
        ];
    }

    public static function labels(): array
    {
        return [
            'id'            => \Yii::t('yiitrix', 'ID'),
            'content_type'  => \Yii::t('yiitrix', 'Content type'),
            'original_name' => \Yii::t('yiitrix', 'Original name'),
            'path'          => \Yii::t('yiitrix', 'Path to file'),
            'uri'           => \Yii::t('yiitrix', 'Web uri address to file'),
            'created_at'    => \Yii::t('yiitrix', 'Created at'),
        ];
    }

    public function base64(): ?string
    {
        $file = $this->getRealPath();

        return file_exists($file) && is_readable($file) ? base64_encode(file_get_contents($file)) : null;
    }

    public static function upload(UploadedFile $file): File
    {
        $path = self::uploadPath($file->getExtension());

        if ($file->saveAs($path) === false) {
            throw new InvalidCallException(__METHOD__ . ": can not upload file in path {$path}");
        }

        $webRoot = \Yii::getAlias('@webroot');
        $uri     = strpos($path, $webRoot) === 0 ? str_replace($webRoot, '', $path) : null;
        $path    = str_replace(APP_ROOT, '@app', $path);

        return new static([
            'content_type'  => $file->type,
            'original_name' => $file->name,
            'path'          => $path,
            'uri'           => $uri,
        ]);
    }

    public static function byPath(string $source): File
    {
        $file = \Yii::getAlias($source);
        $path = self::uploadPath(pathinfo($file, PATHINFO_EXTENSION));
        $name = basename($file);
        $type = mime_content_type($file);

        copy($file, $path);

        $webRoot = \Yii::getAlias('@webroot');
        $uri     = strpos($path, $webRoot) === 0 ? str_replace($webRoot, '', $path) : null;
        $path    = str_replace(APP_ROOT, '@app', $path);

        return new static([
            'content_type'  => $type,
            'original_name' => $name,
            'path'          => $path,
            'uri'           => $uri,
        ]);
    }

    public static function uploadPath(string $extension, string $prefix = null): string
    {
        $extension = mb_strtolower($extension);

        do {
            $name = rtrim(md5((string)random_int(0, PHP_INT_MAX)) . '.' . $extension, '.');
            $path = preg_replace('#//+#', '/', \Yii::getAlias("@upload/{$prefix}/{$name[0]}/{$name[1]}{$name[2]}/{$name}"));
        } while (file_exists($path));

        FileHelper::createDirectory(dirname($path), 02775);

        return $path;
    }
}
