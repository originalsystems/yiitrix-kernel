<?php

declare(strict_types=1);

namespace yiitrix\models;

use yii\db\ActiveQuery;
use yii\web\IdentityInterface;
use yii2kernel\behaviors\HashBehavior;
use yii2kernel\behaviors\JsonBehavior;
use yii2kernel\behaviors\TimestampBehavior;
use yii2kernel\db\ActiveRecord;

/**
 * @property int    $id
 * @property int    $image_id
 * @property int    $status
 * @property string $code
 * @property string $login
 * @property string $email
 * @property string $fields
 * @property int    $created_at
 * @property int    $updated_at
 *
 * @property File   $image
 *
 * @property string $statusLabel
 *
 * @see User::behaviors()
 * @see JsonBehavior::$properties
 *
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $auth_key
 * @property string $access_token
 */
class User extends ActiveRecord implements IdentityInterface
{
    public const STATUS_ACTIVE   = 'active';
    public const STATUS_INACTIVE = 'inactive';
    public const STATUS_DELETED  = 'deleted';

    public $json = [];

    public static function tableName(): string
    {
        return 'public.user';
    }

    public static function statusLabels(): array
    {
        return [
            self::STATUS_ACTIVE   => \Yii::t('yiitrix', 'Active'),
            self::STATUS_INACTIVE => \Yii::t('yiitrix', 'Inactive'),
            self::STATUS_DELETED  => \Yii::t('yiitrix', 'Deleted'),
        ];
    }

    public static function findIdentity($id)
    {
        return static::find()->andWhere([
            'id'     => $id,
            'status' => self::STATUS_ACTIVE,
        ])->one();
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        return self::find()->andWhere([
            '"user"."access_token"' => $token,
            '"user"."status"'       => self::STATUS_ACTIVE,
        ])->one();
    }

    public function getId()
    {
        return $this->getPrimaryKey();
    }

    public function getAuthKey(): string
    {
        return $this->auth_key;
    }

    public function validateAuthKey($authKey): bool
    {
        return $this->getAuthKey() === $authKey;
    }

    public function behaviors(): array
    {
        return [
            TimestampBehavior::class,
            [
                'class'      => JsonBehavior::class,
                'map'        => [
                    'fields' => 'json',
                ],
                'properties' => [
                    'auth_key'             => ['json', 'security.auth_key'],
                    'password_hash'        => ['json', 'security.password_hash'],
                    'password_reset_token' => ['json', 'security.password_reset_token'],
                    'access_token'         => ['json', 'security.access_token'],
                ],
            ],
            [
                'class'     => HashBehavior::class,
                'length'    => 12,
                'attribute' => 'code',
            ],
        ];
    }

    public function rules(): array
    {
        $statusLabels = self::statusLabels();

        return [
            [['status', 'email', 'auth_key'], 'required'],
            [['image_id'], 'integer'],
            [
                ['image_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => File::class,
                'targetAttribute' => 'id',
            ],
            [['status'], 'in', 'range' => array_keys($statusLabels)],
            [['email'], 'email'],
            [['login', 'email'], 'unique'],
            [
                ['password_reset_token'],
                'unique',
                'filter' => function (\yii\db\Query $query) {
                    $query->where('"public"."user"."fields" -> \'security\' ->> \'password_reset_token\' = :PASSWORD_RESET_TOKEN', [
                        ':PASSWORD_RESET_TOKEN' => $this->password_reset_token,
                    ]);
                },
            ],
            [
                ['access_token'],
                'unique',
                'filter' => function (\yii\db\Query $query) {
                    $query->where('"public"."user"."fields" -> \'security\' ->> \'access_token\' = :ACCESS_TOKEN', [
                        ':ACCESS_TOKEN' => $this->access_token,
                    ]);
                },
            ],
        ];
    }

    public static function labels(): array
    {
        return [
            'id'                   => \Yii::t('yiitrix', 'ID'),
            'image'                => \Yii::t('yiitrix', 'Image'),
            'image_id'             => \Yii::t('yiitrix', 'Image'),
            'status'               => \Yii::t('yiitrix', 'Status'),
            'name'                 => \Yii::t('yiitrix', 'Name'),
            'login'                => \Yii::t('yiitrix', 'Login'),
            'email'                => \Yii::t('yiitrix', 'E-mail'),
            'auth_key'             => \Yii::t('yiitrix', 'Authorization key'),
            'password_hash'        => \Yii::t('yiitrix', 'Password hash'),
            'password_reset_token' => \Yii::t('yiitrix', 'Password reset token'),
            'access_token'         => \Yii::t('yiitrix', 'Access token'),
            'created_at'           => \Yii::t('yiitrix', 'Created at'),
            'updated_at'           => \Yii::t('yiitrix', 'Updated at'),
        ];
    }

    public function getImage(): ActiveQuery
    {
        return $this->hasOne(File::class, ['id' => 'image_id'])->andWhere(['content_type' => File::$imageMimeTypes])->andWhere('uri IS NOT NULL');
    }

    public function getStatusLabel(): ?string
    {
        $statusLabels = self::statusLabels();

        return $statusLabels[$this->status] ?? null;
    }
}
