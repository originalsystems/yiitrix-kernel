<?php

declare(strict_types=1);

namespace yiitrix\models\catalog;

use yii\base\Behavior;
use yii\base\Event;
use yii\db\ActiveRecord;
use yii2kernel\behaviors\JsonBehavior;
use yii2kernel\behaviors\TimestampBehavior;

/**
 * @property string $code
 * @property string $name
 * @property string $created_at
 */
class Currency extends \yii2kernel\db\ActiveRecord
{
    public $json = [];

    public static function tableName(): string
    {
        return 'catalog.currency';
    }

    public function behaviors(): array
    {
        return [
            TimestampBehavior::class,
            [
                'class' => JsonBehavior::class,
                'map'   => [
                    'fields' => 'json',
                ],
            ],
        ];
    }

    public function rules(): array
    {
        return [
            [['code'], 'required'],
            [['code'], 'string', 'max' => 16],
        ];
    }

    public static function labels(): array
    {
        return [
            'code'       => \Yii::t('yiitrix', 'Code'),
            'name'       => \Yii::t('yiitrix', 'Name'),
            'created_at' => \Yii::t('yiitrix', 'Created at'),
        ];
    }
}
