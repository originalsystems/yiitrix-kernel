<?php

declare(strict_types=1);

namespace yiitrix\models\record;

use yii2kernel\behaviors\JsonBehavior;
use yii2kernel\behaviors\TimestampBehavior;
use yii2kernel\db\ActiveRecord;

/**
 * @property string $code
 * @property bool   $is_catalog
 * @property string $name
 * @property string $fields
 * @property int    $sort
 * @property string $created_at
 * @property string $updated_at
 */
class Block extends ActiveRecord
{
    public $json = [];

    public static function tableName(): string
    {
        return 'record.block';
    }

    public function behaviors(): array
    {
        return [
            TimestampBehavior::class,
            [
                'class' => JsonBehavior::class,
                'map'   => [
                    'fields' => 'json',
                ],
            ],
        ];
    }

    public function rules(): array
    {
        return [
            [['code', 'is_catalog', 'name', 'sort'], 'required'],
            [['code'], 'string', 'max' => 64],
            [['code'], 'unique'],
            [['is_catalog'], 'boolean'],
            [['name'], 'string', 'max' => 255],
            [['sort'], 'integer'],
        ];
    }

    public static function labels(): array
    {
        return [
            'code'       => \Yii::t('yiitrix', 'Code'),
            'is_catalog' => \Yii::t('yiitrix', 'Is catalog'),
            'name'       => \Yii::t('yiitrix', 'Name'),
            'fields'     => \Yii::t('yiitrix', 'Fields'),
            'sort'       => \Yii::t('yiitrix', 'Sort'),
            'created_at' => \Yii::t('yiitrix', 'Created at'),
            'updated_at' => \Yii::t('yiitrix', 'Updated at'),
        ];
    }
}
