<?php

declare(strict_types=1);

namespace yiitrix\models\record;

use yii\caching\DbDependency;
use yii2kernel\behaviors\JsonBehavior;
use yii2kernel\behaviors\TimestampBehavior;
use yii2kernel\db\ActiveQuery;
use yii2kernel\db\ActiveRecord;

/**
 * @property string  $code
 * @property string  $block_code
 * @property string  $section_code
 * @property bool    $is_active
 * @property string  $name
 * @property string  $fields
 * @property int     $sort
 * @property string  $created_at
 * @property string  $updated_at
 *
 * @property Block   $block
 * @property Section $parentSection
 */
class Section extends ActiveRecord
{
    public $json = [];

    public static function tableName(): string
    {
        return 'record.section';
    }

    public function behaviors(): array
    {
        return [
            TimestampBehavior::class,
            [
                'class' => JsonBehavior::class,
                'map'   => [
                    'fields' => 'json',
                ],
            ],
        ];
    }

    public function rules(): array
    {
        return [
            [['code', 'block_code', 'is_active', 'name', 'sort'], 'required'],
            [['code', 'block_code'], 'string', 'max' => 64],
            [['code', 'block_code'], 'unique', 'targetAttribute' => ['code', 'block_code']],
            [['is_active'], 'boolean'],
            [['name'], 'string', 'max' => 255],
            [['sort'], 'integer'],
        ];
    }

    public static function labels(): array
    {
        return [
            'code'         => \Yii::t('yiitrix', 'Code'),
            'block_code'   => \Yii::t('yiitrix', 'Block code'),
            'section_code' => \Yii::t('yiitrix', 'Section code'),
            'is_active'    => \Yii::t('yiitrix', 'Is active'),
            'name'         => \Yii::t('yiitrix', 'Name'),
            'fields'       => \Yii::t('yiitrix', 'Fields'),
            'sort'         => \Yii::t('yiitrix', 'Sort'),
            'created_at'   => \Yii::t('yiitrix', 'Created at'),
            'updated_at'   => \Yii::t('yiitrix', 'Updated at'),
        ];
    }

    public function getBlock(): \yii2kernel\db\ActiveQuery
    {
        /** @noinspection PhpIncompatibleReturnTypeInspection */
        return $this->hasOne(Block::class, ['code' => 'block_code']);
    }

    public function getParentSection(): \yii2kernel\db\ActiveQuery
    {
        /** @noinspection PhpIncompatibleReturnTypeInspection */
        return $this->hasOne(__CLASS__, ['code' => 'section_code', 'block_code' => 'block_code']);
    }
}
