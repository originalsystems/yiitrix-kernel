<?php

declare(strict_types=1);

namespace yiitrix\modules\admin;

use yii\base\Application;
use yii\base\BootstrapInterface;
use yii\filters\AccessControl;
use yiitrix\rbac\AuthManager;

class Module extends \yii\base\Module implements BootstrapInterface
{
    public $layout              = '@yiitrix/modules/admin/views/layouts/main';
    public $viewPath            = '@yiitrix/modules/admin/views';
    public $controllerNamespace = 'yiitrix\\modules\\admin\\controllers';

    public function behaviors(): array
    {
        return [
            'access' => [
                'class'  => AccessControl::class,
                'except' => [
                    'default/error',
                    'auth/sign-in',
                ],
                'rules'  => [
                    [
                        'allow' => true,
                        'roles' => [AuthManager::ENTER_BACKEND],
                    ],
                ],
            ],
        ];
    }

    public function init(): void
    {
        parent::init();

        \Yii::$app->getUser()->loginUrl            = ['/admin/auth/sign-in'];
        \Yii::$app->getErrorHandler()->errorAction = '/admin/default/error';
    }

    /**
     * @param \yii\base\Application $app
     */
    public function bootstrap($app)
    {
        $app->getUrlManager()->addRules($this->rules(), false);
    }

    private function rules(): array
    {
        $rawRules = [
            'admin'                                     => 'admin/default/index',
            'admin/<controller:[\w-]+>'                 => 'admin/<controller>/index',
            'admin/<controller:[\w-]+>/<action:[\w-]+>' => 'admin/<controller>/<action>',
        ];

        $rules = [];

        foreach ($rawRules as $rule => $route) {
            $rules["<_lang:(\w\w)>/{$rule}"] = $route;
            $rules[$rule]                    = $route;
        }

        return $rules;
    }
}
