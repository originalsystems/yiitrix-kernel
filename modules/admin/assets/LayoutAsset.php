<?php

declare(strict_types=1);

namespace yiitrix\modules\admin\assets;

use yii\web\YiiAsset;
use yii\bootstrap\BootstrapAsset;
use yii2kernel\assets\FileDependAsset;
use yii2kernel\assets\FontAwesomeAsset;
use yii2kernel\assets\FontsAsset;
use yii2kernel\assets\RemodalAsset;
use yiitrix\web\assets\AppAsset;

class LayoutAsset extends FileDependAsset
{
    public $sourcePath = '@yiitrix/modules/admin/assets/layout';
    public $depends    = [
        YiiAsset::class,
        BootstrapAsset::class,
        FontAwesomeAsset::class,
        FontsAsset::class,
        AppAsset::class,
        RemodalAsset::class,
    ];

    public $js = [
        'modals.js',
    ];
}
