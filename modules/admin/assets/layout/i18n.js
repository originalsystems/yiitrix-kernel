jQuery(function() {
    'use strict';

    let $container   = jQuery('.section-i18n-grid'),
        $grid_form   = jQuery('#i18n-grid-form'),
        $filter_form = jQuery('#i18n-grid-filter');

    $container.on('change keydown keyup', 'td.input-cell input', function(e) {
        let $this = jQuery(this),
            $cell = $this.closest('td.input-cell');

        $cell.toggleClass('changed', $this.val() !== $this.attr('value'));
        $container.toggleClass('changed', $container.find('td.input-cell.changed').length !== 0);
    });

    $grid_form.on('reset', function(e) {
        $container.removeClass('changed');
        $container.find('td.input-cell').removeClass('changed');
    });

    $grid_form.on('submit', function(e) {
        e.preventDefault();

        let data = {};

        $grid_form.serializeArray().forEach((pair) => {
            data[pair.name] = pair.value;
        });

        App.post($grid_form.attr('action'), data).then(function() {
            $container.removeClass('changed');

            update();
        });
    });

    $filter_form.on('beforeSubmit', function(e) {
        update();

        return false;
    });

    $filter_form.on('keyup', 'input, textarea', function(e) {
        if (e.which === 13) {
            $filter_form.submit();
        }
    });

    function update() {
        let url   = location.origin + location.pathname,
            query = App.search_values_with_form($filter_form);

        if (Object.keys(query).length !== 0) {
            url += '?' + decodeURIComponent(jQuery.param(query));
        }

        jQuery.pjax({
            url:       url,
            container: '#i18n-grid-pjax'
        });
    }
});
