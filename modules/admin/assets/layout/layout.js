jQuery(function() {
    'use strict';

    let $body   = jQuery('body'),
        $header = jQuery('header'),
        $aside  = jQuery('aside');

    jQuery(document).on('click', '[data-toggle="menu"], body.menu-opened main', function(e) {
        e.preventDefault();

        if ($body.hasClass('menu-opened-process')) {
            return;
        }

        $body.addClass('menu-opened-process');

        setTimeout(function() {

            $body.toggleClass('menu-opened');
        }, 50);

        setTimeout(function() {
            $body.removeClass('menu-opened-process');
        }, 350);
    });

    jQuery(document).on('click', 'ul.navigation-menu li.dropdown > a', function(e) {
        e.preventDefault();

        let $this          = jQuery(this),
            $dropdown      = $this.closest('li.dropdown'),
            $dropdown_menu = $dropdown.find('> ul.dropdown-menu');

        if ($dropdown.is('.disabled')) {
            return;
        }

        $dropdown_menu.slideToggle(200);
    });

    jQuery(document).on('click', 'ul.navigation-menu li.disabled > a', function(e) {
        e.preventDefault();
    });
});
