jQuery(function() {
    (function() {
        let $modal  = jQuery('.section-block-modal-form'),
            $header = $modal.find('.header'),
            $form   = $modal.find('form'),
            modal   = $modal.remodal();

        if ($modal.length === 0) {
            return;
        }

        jQuery(document).on('click', '[data-role="block-create"]', function(e) {
            e.preventDefault();

            let $this = jQuery(this);

            $header.text($header.data('create-text'));
            modal.open();
        });

        jQuery(document).on('click', '[data-role="block"][data-key] [data-role="block-update"]', function(e) {
            e.preventDefault();

            let $this  = jQuery(this),
                $block = $this.closest('[data-role="block"][data-key]'),
                code   = $block.data('key');

            $header.text($header.data('update-text'));
            modal.open();
        });
    })();
});