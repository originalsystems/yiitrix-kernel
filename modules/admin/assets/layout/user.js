jQuery(function() {
    'use strict';

    (function() {
        let $section     = jQuery('.section-user-form'),
            $form        = $section.find('form'),
            $img_wrapper = $section.find('.img-wrapper'),
            $img         = $img_wrapper.find('.img');

        if ($section.length === 0) {
            return;
        }

        $form.on('change', '.img-wrapper input[type="file"]', function(e) {
            if (this.files.length !== 0) {
                let reader = new FileReader();

                reader.addEventListener('load', function() {
                    $img.css('background-image', 'url(' + this.result + ')');
                });

                reader.readAsDataURL(this.files[0]);
            } else {
                let background = $img.data('background');

                $img.css('background-image', background ? 'url(' + background + ')' : null);
            }
        });

        $form.on('change', '.img-wrapper label input[type="checkbox"][data-role="delete-image"]', function(e) {
            let $this  = jQuery(this),
                $label = $this.closest('label');

            $label.toggleClass('btn-default', !this.checked).toggleClass('btn-danger', this.checked);
            $img_wrapper.toggleClass('delete', this.checked);
        });
    })();
});
