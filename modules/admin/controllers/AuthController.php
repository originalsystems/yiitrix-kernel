<?php

declare(strict_types=1);

namespace yiitrix\modules\admin\controllers;

use yii2kernel\web\Controller;
use yiitrix\modules\admin\forms\SignIn;

class AuthController extends Controller
{
    public $layout = 'empty';

    public function actionSignIn()
    {
        $formModel = new SignIn();

        if ($formModel->load(\Yii::$app->request->post())) {
            if ($formModel->save()) {
                return $this->goBack(['/admin/default/index']);
            }

            $formModel->flashError();
        }

        return $this->render('login', [
            'formModel' => $formModel,
        ]);
    }

    public function actionSignOut()
    {
        \Yii::$app->getUser()->logout();

        return $this->redirect(['/admin/auth/sign-in']);
    }
}
