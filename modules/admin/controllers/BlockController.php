<?php

declare(strict_types=1);

namespace yiitrix\modules\admin\controllers;

use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use yii2kernel\web\Controller;
use yiitrix\models\record\Block as Model;
use yiitrix\models\record\Section;
use yiitrix\modules\admin\forms\Block as Form;
use yiitrix\modules\admin\forms\Section as SectionForm;
use yiitrix\modules\admin\search\Block as Search;
use yiitrix\modules\admin\search\Section as SectionSearch;

class BlockController extends Controller
{
    public function behaviors(): array
    {
        return [
            'verbs' => [
                'class'   => VerbFilter::class,
                'actions' => [
                    'index'  => ['GET', 'HEAD'],
                    'form'   => ['GET', 'POST', 'HEAD'],
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel  = new Search();
        $dataProvider = $searchModel->search(\Yii::$app->getRequest()->getQueryParams());

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionForm(string $block = null)
    {
        $blockModel = $block !== null ? $this->findModel($block) : new Model();
        $formModel  = new Form($blockModel);

        $this->ajaxValidation($formModel);

        if ($formModel->load(\Yii::$app->getRequest()->getBodyParams())) {
            if ($formModel->save()) {
                return $this->redirect(['index']);
            }
        }

        return $this->render('form', [
            'blockModel' => $blockModel,
            'formModel'  => $formModel,
        ]);
    }

    public function actionSections(string $block, string $section = null)
    {
        $blockModel   = $this->findModel($block);
        $sectionModel = $section !== null ? $this->findSection($block, $section) : null;
        $searchModel  = new SectionSearch();
        $formModel    = new SectionForm($blockModel, $sectionModel ?? new Section());

        $searchModel->block_code   = $block;
        $searchModel->section_code = $section;

        $dataProvider = $searchModel->search(\Yii::$app->getRequest()->getQueryParams());

        return $this->render('sections', [
            'blockModel'   => $blockModel,
            'sectionModel' => $sectionModel,
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
            'formModel'    => $formModel,
        ]);
    }

    public function actionFormSection(string $block, string $section = null, string $section_code = null)
    {
        $blockModel   = $this->findModel($block);
        $sectionModel = $section !== null ? $this->findSection($block, $section) : new Section(['section_code' => $section_code]);
        $formModel    = new SectionForm($blockModel, $sectionModel);

        $this->ajaxValidation($formModel);

        if ($formModel->load(\Yii::$app->getRequest()->getBodyParams())) {
            if ($formModel->save()) {
                return $this->redirect(['sections', 'block' => $block, 'section' => $sectionModel->section_code]);
            }
        }

        return $this->render('form-section', [
            'blockModel'   => $blockModel,
            'sectionModel' => $sectionModel,
            'formModel'    => $formModel,
        ]);
    }

    public function actionDelete(string $block)
    {
        $blockModel = $this->findModel($block);

        $blockModel->delete();

        return $this->redirect(['index']);
    }

    protected function findModel(string $block): Model
    {
        /** @noinspection PhpIncompatibleReturnTypeInspection */
        return Model::find()->andWhere(['code' => $block])->needOne();
    }

    protected function findSection(string $block, string $section, string $section_code = null): Section
    {
        $query = Section::find();

        $query->andWhere([
            'block_code' => $block,
            'code'       => $section,
        ]);

        if ($section_code !== null) {
            $query->andWhere(['section_code' => $section_code]);
        }

        /**
         * @var Section $model
         */
        $model = $query->one();

        if ($model === null) {
            throw new NotFoundHttpException(\Yii::t('yiitrix', 'Can not find requested section'));
        }

        return $model;
    }
}
