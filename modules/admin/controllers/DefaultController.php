<?php

declare(strict_types=1);

namespace yiitrix\modules\admin\controllers;

use yii\web\ErrorAction;
use yii2kernel\web\Controller;

class DefaultController extends Controller
{
    /**
     * @param \yii\base\Action $action
     *
     * @return bool
     */
    public function beforeAction($action): bool
    {
        if ($action->id === 'error') {
            $this->layout = 'empty';
        }

        return parent::beforeAction($action);
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => ErrorAction::class,
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }
}
