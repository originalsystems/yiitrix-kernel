<?php

declare(strict_types=1);

namespace yiitrix\modules\admin\controllers;

use yii2kernel\crud\Controller;
use yiitrix\models\User as Model;
use yiitrix\modules\admin\search\User as Search;
use yiitrix\modules\admin\forms\User as Form;

class UserController extends Controller
{
    public $modelClass  = Model::class;
    public $searchClass = Search::class;
    public $formClass   = Form::class;
    public $indexView   = '@yiitrix/modules/admin/views/user/index';
    public $formView    = '@yiitrix/modules/admin/views/user/form';
}
