<?php

declare(strict_types=1);

namespace yiitrix\modules\admin\forms;

use yii\base\Exception;
use yii\db\ActiveQuery;
use yii\helpers\Html;
use yii2kernel\base\FormModel;
use yiitrix\models\record\Block as Model;

class Block extends FormModel
{
    public $code;
    public $name;
    public $is_catalog;
    public $sort;

    /**
     * @var \yiitrix\models\record\Block
     */
    private $_model;

    public function __construct(Model $model, array $config = [])
    {
        $this->code       = $model->code;
        $this->name       = $model->name;
        $this->is_catalog = $model->is_catalog;
        $this->sort       = $model->sort ? : 100;

        $this->_model = $model;

        parent::__construct($config);
    }

    public function rules(): array
    {
        return [
            [['sort'], 'default', 'value' => 100],
            [['code', 'name', 'is_catalog', 'sort'], 'required'],
            [['code'], 'string', 'max' => 64],
            [
                ['code'],
                'unique',
                'targetClass' => Model::class,
                'filter'      => function (ActiveQuery $query) {
                    if ($this->_model->getIsNewRecord() === false) {
                        $query->andWhere(['NOT', ['code' => $this->_model->code]]);
                    }
                },
            ],
            [['name'], 'string', 'max' => 255],
            [['is_catalog'], 'boolean'],
            [['is_catalog'], 'filter', 'filter' => 'boolval'],
            [['sort'], 'integer'],
        ];
    }

    public static function labels(): array
    {
        return [
            'code'       => \Yii::t('yiitrix', 'Code'),
            'is_catalog' => \Yii::t('yiitrix', 'Is catalog'),
            'name'       => \Yii::t('yiitrix', 'Name'),
            'sort'       => \Yii::t('yiitrix', 'Sort'),
        ];
    }

    public function save(): bool
    {
        if ($this->validate() === false) {
            return false;
        }

        $model       = $this->_model;
        $transaction = \Yii::$app->getDb()->beginTransaction();

        try {
            $model->code       = $this->code;
            $model->name       = $this->name;
            $model->is_catalog = $this->is_catalog;
            $model->sort       = $this->sort;

            $model->strictSave();

            $transaction->commit();
        } catch (Exception $ex) {
            $transaction->rollBack();
            $this->addErrors($model->getErrors());

            return false;
        }

        return true;
    }
}
