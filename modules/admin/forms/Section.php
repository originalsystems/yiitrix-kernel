<?php

declare(strict_types=1);

namespace yiitrix\modules\admin\forms;

use yii\base\Exception;
use yii\db\ActiveQuery;
use yii\helpers\Html;
use yii2kernel\base\FormModel;
use yiitrix\models\record\Block;
use yiitrix\models\record\Section as Model;

class Section extends FormModel
{
    public $code;
    public $block_code;
    public $section_code;
    public $is_active;
    public $name;
    public $sort;

    private $_model;

    public function __construct(Block $block, Model $model)
    {
        $this->code         = $model->code;
        $this->block_code   = $block->code;
        $this->section_code = $model->section_code;
        $this->is_active    = $model->is_active ?? true;
        $this->name         = $model->name;
        $this->sort         = $model->sort ?? 100;

        $this->_model = $model;

        parent::__construct();
    }

    public function rules(): array
    {
        return [
            [['sort'], 'default', 'value' => 100],
            [['code', 'block_code', 'name', 'is_active', 'sort'], 'required'],
            [['code', 'section_code'], 'string', 'max' => 64],
            [
                ['code'],
                'unique',
                'targetClass' => Model::class,
                'filter'      => function (ActiveQuery $query) {
                    if ($this->_model->getIsNewRecord() === false) {
                        $query->andWhere(['NOT', ['code' => $this->_model->code]]);
                    }
                },
            ],
            [
                ['block_code'],
                'exist',
                'targetClass'     => \yiitrix\models\record\Block::class,
                'targetAttribute' => 'code',
            ],
            [
                ['section_code'],
                'exist',
                'targetClass'     => \yiitrix\models\record\Section::class,
                'targetAttribute' => 'code',
            ],
            [['name'], 'string', 'max' => 255],
            [
                ['is_active'],
                'boolean',
                'trueValue'  => Html::TRUE_VALUE,
                'falseValue' => Html::FALSE_VALUE,
            ],
            [['is_active'], 'filter', 'filter' => 'boolval'],
            [['sort'], 'integer'],
        ];
    }

    public static function labels(): array
    {
        return [
            'code'         => \Yii::t('yiitrix', 'Code'),
            'block_code'   => \Yii::t('yiitrix', 'Block code'),
            'section_code' => \Yii::t('yiitrix', 'Section code'),
            'is_active'    => \Yii::t('yiitrix', 'Is active'),
            'name'         => \Yii::t('yiitrix', 'Name'),
            'sort'         => \Yii::t('yiitrix', 'Sort'),
        ];
    }

    public function save(): bool
    {
        if ($this->validate() === false) {
            return false;
        }

        $model       = $this->_model;
        $transaction = \Yii::$app->getDb()->beginTransaction();

        try {
            $model->code         = $this->code;
            $model->block_code   = $this->block_code;
            $model->section_code = $this->section_code ? : null;
            $model->is_active    = $this->is_active;
            $model->name         = $this->name;
            $model->sort         = $this->sort;

            $model->strictSave();

            $transaction->commit();
        } catch (Exception $ex) {
            $transaction->rollBack();
            $this->addErrors($model->getErrors());

            return false;
        }

        return true;
    }
}
