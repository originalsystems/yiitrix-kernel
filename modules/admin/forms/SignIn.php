<?php

declare(strict_types=1);

namespace yiitrix\modules\admin\forms;

use yii2kernel\base\FormModel;
use yiitrix\models\User;
use yiitrix\rbac\AuthManager;

class SignIn extends FormModel
{
    public $login;
    public $password;
    public $rememberMe = true;

    public function rules(): array
    {
        return [
            [['login', 'password'], 'required'],
            [['rememberMe'], 'boolean'],
            [['password'], 'validatePassword'],
        ];
    }

    public static function labels(): array
    {
        return [
            'login'      => \Yii::t('yiitrix', 'Login'),
            'password'   => \Yii::t('yiitrix', 'Password'),
            'rememberMe' => \Yii::t('yiitrix', 'Remember me'),
        ];
    }

    public function validatePassword($attribute, $params): bool
    {
        if ($this->hasErrors()) {
            return false;
        }

        $user = $this->getUser();

        if ($user === null) {
            $this->addError($attribute, \Yii::t('yiitrix', 'Invalid login or password.'));

            return false;
        }

        if (\Yii::$app->getAuthManager()->checkAccess($user->getId(), AuthManager::ENTER_BACKEND) === false) {
            $this->addError($attribute, \Yii::t('yiitrix', 'Invalid login or password.'));

            return false;
        }

        try {
            $validPassword = \Yii::$app->getSecurity()->validatePassword($this->password, $user->password_hash);
        } catch (\Exception $ex) {
            $validPassword = false;
        }

        if ($validPassword === false) {
            $this->addError($attribute, \Yii::t('yiitrix', 'Invalid login or password.'));

            return false;
        }

        return true;
    }

    /**
     * @return boolean
     * @throws \yii\base\InvalidParamException
     */
    public function save(): bool
    {
        if ($this->validate() === false) {
            return false;
        }

        /**
         * Log in on month in case of checked "rememberMe"
         */
        return \Yii::$app->getUser()->login($this->getUser(), $this->rememberMe ? 2592000 : 0);
    }

    /**
     * @var User
     */
    private $_user = false;

    /**
     * @return User|null
     */
    protected function getUser(): ?\yiitrix\models\User
    {
        if ($this->_user === false) {
            $this->_user = User::find()->where([
                'login'  => $this->login,
                'status' => User::STATUS_ACTIVE,
            ])->one();
        }

        return $this->_user;
    }
}
