<?php

declare(strict_types=1);

namespace yiitrix\modules\admin\forms;

use yii\base\Exception;
use yii\db\ActiveQuery;
use yii\helpers\Html;
use yii\web\UploadedFile;
use yii2kernel\base\FormModel;
use yiitrix\rbac\AuthManager;
use yiitrix\models\File;
use yiitrix\models\User as Model;

class User extends FormModel
{
    public $login;
    public $status;
    public $email;
    public $password;
    public $password_repeat;
    public $roles = [];

    public $image;
    public $delete_image;

    /**
     * @var \yiitrix\models\User
     */
    protected $_model;

    public function __construct(\yiitrix\models\User $model, array $config = [])
    {
        $this->login  = $model->login;
        $this->status = $model->status;
        $this->email  = $model->email;

        foreach (\Yii::$app->getAuthManager()->getRolesByUser($model->id) as $role) {
            $this->roles[] = $role->name;
        }

        $this->_model = $model;

        parent::__construct($config);
    }

    public function rules(): array
    {
        $statusLabels = Model::statusLabels();
        $roleLabels   = AuthManager::roleLabels();

        return [
            [['status', 'login', 'email'], 'required'],
            [['status'], 'in', 'range' => array_keys($statusLabels)],
            [['roles'], 'each', 'rule' => ['in', 'range' => array_keys($roleLabels)]],
            [['email'], 'email'],
            [
                ['login', 'email'],
                'unique',
                'targetClass' => Model::class,
                'filter'      => function (ActiveQuery $query) {
                    if ($this->_model->getIsNewRecord() === false) {
                        $query->andWhere(['NOT', ['id' => $this->_model->id]]);
                    }
                },
            ],
            [
                ['password'],
                'required',
                'when' => function () {
                    return $this->_model->getIsNewRecord();
                },
            ],
            [['password'], 'string', 'min' => 6],
            [['password_repeat'], 'compare', 'compareAttribute' => 'password', 'skipOnEmpty' => false],
            [['roles'], 'validateRoles', 'skipOnEmpty' => false],
            [['image'], 'imageValidator', 'skipOnEmpty' => false],
            [['delete_image'], 'boolean'],
            [['delete_image'], 'filter', 'filter' => 'boolval'],
        ];
    }

    public function validateRoles($attribute): void
    {
        $roleLabels  = AuthManager::roleLabels();
        $this->roles = array_filter((array)$this->roles);

        foreach ($this->roles as $role) {
            if (array_key_exists($role, $roleLabels) === false) {
                $this->addError($attribute, \Yii::t('yiitrix', 'Invalid value'));

                return;
            }
        }
    }

    public function imageValidator($attribute): void
    {
        $this->image = UploadedFile::getInstance($this, $attribute);
    }

    public static function labels(): array
    {
        return array_merge(Model::labels(), [
            'password'        => \Yii::t('yiitrix', 'Password'),
            'password_repeat' => \Yii::t('yiitrix', 'Repeat password'),
            'roles'           => \Yii::t('yiitrix', 'User roles'),
        ]);
    }

    public function attributeHints(): array
    {
        return [
            'image' => Html::imageHint($this->_model->image),
        ];
    }

    public function save(): bool
    {
        if ($this->validate() === false) {
            return false;
        }

        $model       = $this->_model;
        $security    = \Yii::$app->getSecurity();
        $auth        = \Yii::$app->getAuthManager();
        $transaction = \Yii::$app->getDb()->beginTransaction();

        try {
            $model->login  = $this->login;
            $model->status = $this->status;
            $model->email  = $this->email;

            if ($this->delete_image) {
                $model->image_id = null;
            } elseif ($this->image !== null && $image = File::upload($this->image)) {
                $image->strictSave();

                $model->image_id = $image->id;
            }

            if ($model->getIsNewRecord() === true) {
                $model->auth_key     = $security->generateRandomString(32);
                $model->access_token = $security->generateRandomString(32);
            }

            if (!empty($this->password)) {
                $model->password_hash = $security->generatePasswordHash($this->password);
            }

            $model->strictSave();

            $auth->revokeAll($model->id);

            foreach ($this->roles as $roleName) {
                $role = $auth->getRole($roleName);

                $auth->assign($role, $model->id);
            }

            $transaction->commit();
        } catch (Exception $ex) {
            $transaction->rollBack();
            $this->addErrors($model->getErrors());

            return false;
        }

        return true;
    }
}
