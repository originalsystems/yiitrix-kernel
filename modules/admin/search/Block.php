<?php

declare(strict_types=1);

namespace yiitrix\modules\admin\search;

use yii\data\ActiveDataProvider;
use yii\data\DataProviderInterface;
use yii\helpers\Html;
use yii2kernel\base\SearchModel;

class Block extends \yiitrix\models\record\Block implements SearchModel
{
    public function rules(): array
    {
        return [
            [['code'], 'string', 'max' => 64],
            [['name'], 'string', 'max' => 255],
            [
                ['is_catalog'],
                'boolean',
                'trueValue'  => Html::TRUE_VALUE,
                'falseValue' => Html::FALSE_VALUE,
            ],
        ];
    }

    public function search(array $data = []): DataProviderInterface
    {
        $query        = static::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'  => [
                'defaultOrder' => ['sort' => SORT_ASC],
            ],
        ]);

        $this->load($data);

        if ($this->validate() === false) {
            $query->where('0=1');

            return $dataProvider;
        }

        $query->andFilterWhere([
            '"record"."block"."sort"' => $this->sort,
        ]);

        if ($this->is_catalog === Html::TRUE_VALUE) {
            $query->andWhere('"record"."block"."is_catalog" = TRUE');
        }

        if ($this->is_catalog === Html::FALSE_VALUE) {
            $query->andWhere('"record"."block"."is_catalog" = FALSE');
        }

        $query->andFilterLike('"record"."block"."code"', $this->code);
        $query->andFilterLike('"record"."block"."name"', $this->name);

        return $dataProvider;
    }
}
