<?php

declare(strict_types=1);

namespace yiitrix\modules\admin\search;

use yii\data\ActiveDataProvider;
use yii\data\DataProviderInterface;
use yii\helpers\Html;
use yii2kernel\base\SearchModel;

class Section extends \yiitrix\models\record\Section implements SearchModel
{
    public function rules(): array
    {
        return [
            [['code'], 'string', 'max' => 64],
            [['name'], 'string', 'max' => 255],
            [
                ['is_active'],
                'boolean',
                'trueValue'  => Html::TRUE_VALUE,
                'falseValue' => Html::FALSE_VALUE,
            ],
        ];
    }

    public function search(array $data = []): DataProviderInterface
    {
        $query        = static::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'  => [
                'defaultOrder' => ['sort' => SORT_ASC],
            ],
        ]);

        $this->load($data);

        if ($this->validate() === false) {
            $query->where('0=1');

            return $dataProvider;
        }

        $query->andWhere([
            '"record"."section"."block_code"'   => $this->block_code,
            '"record"."section"."section_code"' => $this->section_code,
        ]);

        $query->andFilterWhere([
            '"record"."section"."sort"' => $this->sort,
        ]);

        if ($this->is_active === Html::TRUE_VALUE) {
            $query->andWhere('"record"."section"."is_active" = TRUE');
        }

        if ($this->is_active === Html::FALSE_VALUE) {
            $query->andWhere('"record"."section"."is_active" = FALSE');
        }

        $query->andFilterLike('"record"."section"."code"', $this->code);
        $query->andFilterLike('"record"."section"."name"', $this->name);

        return $dataProvider;
    }
}
