<?php

declare(strict_types=1);

namespace yiitrix\modules\admin\search;

use yii\data\ActiveDataProvider;
use yiitrix\models\User as Model;

class User extends Model
{
    public function rules(): array
    {
        $statusLabels = self::statusLabels();

        return [
            [['id'], 'integer'],
            [['status'], 'in', 'range' => array_keys($statusLabels)],
            [['login', 'email'], 'string'],
        ];
    }

    /**
     * @param array $params
     *
     * @return ActiveDataProvider
     * @throws \yii\base\InvalidParamException
     */
    public function search($params): \yii\data\ActiveDataProvider
    {
        $query = self::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'  => [
                'defaultOrder' => ['id' => SORT_ASC],
            ],
        ]);

        $this->load($params);

        if ($this->validate() === false) {
            $query->where('0=1');

            return $dataProvider;
        }

        $query->andFilterWhere([
            '"user"."id"'     => $this->id,
            '"user"."status"' => $this->status,
        ]);

        $query->andFilterLike('"user"."login"', $this->login);
        $query->andFilterLike('"user"."email"', $this->email);

        return $dataProvider;
    }
}
