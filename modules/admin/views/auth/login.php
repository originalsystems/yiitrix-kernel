<?php

/**
 * @var \yii2kernel\web\View                $this
 * @var \yii\bootstrap\ActiveForm           $form
 * @var \yiitrix\modules\admin\forms\SignIn $formModel
 */

declare(strict_types=1);

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->title         = \Yii::t('yiitrix', 'Sign in');
$this->breadcrumbs[] = $this->title;
?>
<div class="site-login">
    <p><?= \Yii::t('yiitrix', 'Fill in your login details for sign in.') ?></p>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

            <?= $form->field($formModel, 'login')->textInput(); ?>

            <?= $form->field($formModel, 'password')->passwordInput(); ?>

            <?= $form->field($formModel, 'rememberMe')->checkbox(); ?>

            <div class="form-group">
                <?= Html::submitButton(\Yii::t('yiitrix', 'Submit'), ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
