<?php

/**
 * @var \yii2kernel\web\View                 $this
 * @var \yiitrix\modules\admin\forms\Section $formModel
 * @var \yiitrix\models\record\Block         $blockModel
 * @var \yiitrix\models\record\Section       $sectionModel
 */

declare(strict_types=1);

use yii\widgets\ActiveForm;

$this->title = $sectionModel->getIsNewRecord() ? \Yii::t('yiitrix', 'Create new section') : \Yii::t('yiitrix', 'Section "{code}"', ['code' => $formModel->code]);

$this->breadcrumbs[] = ['label' => \Yii::t('yiitrix', 'Blocks'), 'url' => ['index']];
$this->breadcrumbs[] = [
    'label' => \Yii::t('yiitrix', 'Block "{code}"', ['code' => $blockModel->code]),
    'url'   => ['form', 'block' => $blockModel->code],
];
$this->breadcrumbs[] = $this->title;
?>
<section class="section-section-form">
    <?php $form = ActiveForm::begin([
        'enableAjaxValidation'   => true,
        'enableClientValidation' => false,
        'fieldConfig'            => [
            'template'     => '<div class="row">{label}<div class="col-sm-9">{input}{error}</div></div>',
            'labelOptions' => [
                'class' => 'control-label col-sm-3',
            ],
        ],
        'options'                => [
            'class' => 'form-horizontal',
        ],
    ]); ?>

    <?= \yii\helpers\Html::activeHiddenInput($formModel, 'block_code'); ?>
    <?= \yii\helpers\Html::activeHiddenInput($formModel, 'section_code'); ?>

    <?= $form->field($formModel, 'code')->textInput(); ?>

    <?= $form->field($formModel, 'name')->textInput(); ?>

    <?= $form->field($formModel, 'sort')->input('number'); ?>

    <div class="form-group">
        <div class="row">
            <div class="col-sm-3"></div>
            <div class="col-sm-9">
                <div class="form-check">
                    <?= \yii\helpers\Html::activeCheckbox($formModel, 'is_active', [
                        'id'    => 'section-is-active-input',
                        'class' => 'form-check-input',
                        'label' => false,
                    ]); ?>

                    <?= \yii\helpers\Html::activeLabel($formModel, 'is_active', [
                        'class' => 'form-check-label',
                        'for'   => 'section-is-active-input',
                    ]) ?>
                </div>
            </div>
        </div>
    </div>

    <div class="form-group">
        <div class="row">
            <div class="col-sm-3"></div>
            <div class="col-sm-9">
                <button type="submit" class="btn btn-primary"><?= Yii::t('yiitrix', 'Submit'); ?></button>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</section>
