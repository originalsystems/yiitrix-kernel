<?php

/**
 * @var \yii2kernel\web\View               $this
 * @var \yiitrix\modules\admin\forms\Block $formModel
 * @var \yiitrix\models\record\Block       $blockModel
 */

declare(strict_types=1);

use yii\widgets\ActiveForm;

$this->title = $blockModel->getIsNewRecord() ? \Yii::t('yiitrix', 'Create new block') : \Yii::t('yiitrix', 'Block "{code}"', ['code' => $formModel->code]);

$this->breadcrumbs[] = ['label' => \Yii::t('yiitrix', 'Blocks'), 'url' => ['index']];
$this->breadcrumbs[] = $this->title;
?>
<section class="section-block-form">
    <?php $form = ActiveForm::begin([
        'enableAjaxValidation'   => true,
        'enableClientValidation' => false,
        'fieldConfig'            => [
            'template'     => '<div class="row">{label}<div class="col-sm-9">{input}{error}</div></div>',
            'labelOptions' => [
                'class' => 'control-label col-sm-3',
            ],
        ],
        'options'                => [
            'class' => 'form-horizontal',
        ],
    ]); ?>

    <?= $form->field($formModel, 'code')->textInput(); ?>

    <?= $form->field($formModel, 'name')->textInput(); ?>

    <?= $form->field($formModel, 'sort')->input('number'); ?>

    <div class="form-group">
        <div class="row">
            <div class="col-sm-3"></div>
            <div class="col-sm-9">
                <div class="form-check">
                    <?= \yii\helpers\Html::activeCheckbox($formModel, 'is_catalog', [
                        'id'    => 'block-is-catalog-input',
                        'class' => 'form-check-input',
                        'label' => false,
                    ]); ?>

                    <?= \yii\helpers\Html::activeLabel($formModel, 'is_catalog', [
                        'class' => 'form-check-label',
                        'for'   => 'block-is-catalog-input',
                    ]) ?>
                </div>
            </div>
        </div>
    </div>

    <div class="form-group">
        <div class="row">
            <div class="col-sm-3"></div>
            <div class="col-sm-9">
                <button type="submit" class="btn btn-primary"><?= Yii::t('yiitrix', 'Submit'); ?></button>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</section>
