<?php

/**
 * @var \yii2kernel\web\View                $this
 * @var \yii\data\ActiveDataProvider        $dataProvider
 * @var \yiitrix\modules\admin\search\Block $searchModel
 */

declare(strict_types=1);

use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;
use yii2kernel\grid\ActionColumn;
use yiitrix\modules\admin\search\Block;

$this->title = \Yii::t('yiitrix', 'Blocks');

$this->breadcrumbs[] = $this->title;
?>
<section class="section-block-index">
    <p>
        <?= Html::a('<i class="fa fa-plus"></i> ' . \Yii::t('yiitrix', 'Create'), ['form'], [
            'class'     => 'btn btn-success',
            'data-role' => 'block-create',
        ]); ?>
    </p>

    <?php Pjax::begin(['id' => 'block-grid-pjax', 'submitEvent' => 'submit change']); ?>

    <?= Html::beginForm(['index', $dataProvider->sort->sortParam => Yii::$app->getRequest()->getQueryParam($dataProvider->sort->sortParam)], 'get', [
        'id'        => 'block-grid-form',
        'data-pjax' => true,
    ]); ?>

    <?= GridView::widget([
        'id'           => 'block-grid',
        'dataProvider' => $dataProvider,
        'rowOptions'   => [
            'data-role' => 'block',
        ],
        'layout'       => '{summary}<div class="table-responsive">{items}</div>{pager}',
        'columns'      => [
            [
                'attribute' => 'code',
                'header'    => Html::activeTextInput($searchModel, 'code', [
                        'placeholder' => $searchModel->getAttributeLabel('code'),
                    ]) . $dataProvider->sort->link('code'),
            ],
            [
                'attribute' => 'is_catalog',
                'format'    => 'boolean',
                'header'    => Html::activeDropDownList($searchModel, 'is_catalog', Html::boolLabels(), [
                        'prompt' => $searchModel->getAttributeLabel('is_catalog'),
                    ]) . $dataProvider->sort->link('is_catalog'),
            ],
            [
                'attribute' => 'name',
                'header'    => Html::activeTextInput($searchModel, 'name', [
                        'placeholder' => $searchModel->getAttributeLabel('name'),
                    ]) . $dataProvider->sort->link('name'),
            ],
            [
                'attribute' => 'sort',
                'header'    => Html::activeInput('number', $searchModel, 'sort', [
                        'placeholder' => $searchModel->getAttributeLabel('sort'),
                    ]) . $dataProvider->sort->link('sort'),
            ],
            [
                'attribute' => 'created_at',
                'format'    => 'datetime',
                'header'    => Html::textInput('created_at', null, [
                        'placeholder' => $searchModel->getAttributeLabel('created_at'),
                        'disabled'    => true,
                    ]) . $dataProvider->sort->link('created_at'),
            ],
            [
                'attribute' => 'updated_at',
                'format'    => 'datetime',
                'header'    => Html::textInput('updated_at', null, [
                        'placeholder' => $searchModel->getAttributeLabel('updated_at'),
                        'disabled'    => true,
                    ]) . $dataProvider->sort->link('updated_at'),
            ],
            [
                'class'    => ActionColumn::class,
                'template' => '<div class="btn-group">{sections}{form}{delete}</div>',
                'buttons'  => [
                    'sections' => function ($url, Block $model, $key) {
                        return Html::a('<i class="fa fa-list"></i>', ['sections', 'block' => $model->code], [
                            'title'      => Yii::t('yiitrix', 'Block sections'),
                            'aria-label' => Yii::t('yiitrix', 'Block sections'),
                            'class'      => 'btn btn-primary',
                            'data-pjax'  => '0',
                        ]);
                    },
                    'form'     => function ($url, Block $model, $key) {
                        return Html::a('<i class="fa fa-pencil"></i>', ['form', 'block' => $model->code], [
                            'title'      => Yii::t('yii', 'Update'),
                            'aria-label' => Yii::t('yii', 'Update'),
                            'class'      => 'btn btn-primary',
                            'data-pjax'  => '0',
                            'data-role'  => 'block-update',
                        ]);
                    },
                    'delete'   => function ($url, Block $model, $key) {
                        return Html::a('<i class="fa fa-times"></i>', ['delete', 'block' => $model->code], [
                            'title'        => Yii::t('yii', 'Delete'),
                            'aria-label'   => Yii::t('yii', 'Delete'),
                            'class'        => 'btn btn-danger',
                            'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                            'data-method'  => 'post',
                            'data-pjax'    => '0',
                        ]);
                    },
                ],
                'options'  => [
                    'width' => 120,
                ],
            ],
        ],
    ]); ?>

    <?= Html::endForm(); ?>

    <?php Pjax::end(); ?>
</section>

<section class="section-block-modal-form modal">
    <?= Html::beginForm(); ?>

    <div class="header"
         data-create-text="<?= Yii::t('app', 'Create new block'); ?>"
         data-update-text="<?= Yii::t('app', 'Update block'); ?>"></div>

    <div class="content">
        <div class="form-group">
            <label for="block-form-code-input"><?= Yii::t('yiitrix', 'Code') ?></label>

            <div class="field">
                <?= Html::textInput('code', '', [
                    'placeholder' => Yii::t('yiitrix', 'Block code'),
                ]); ?>
            </div>
        </div>

        <div class="form-group">
            <label for="block-form-code-input"><?= Yii::t('yiitrix', 'Name') ?></label>

            <div class="field">
                <?= Html::textInput('code', '', [
                    'placeholder' => Yii::t('yiitrix', 'Block name'),
                ]); ?>
            </div>
        </div>
    </div>

    <div class="footer">
        <div class="error"></div>
        <button type="submit"><?= Yii::t('app', 'Submit'); ?></button>
    </div>

    <?= Html::endForm(); ?>
</section>
