<?php

/**
 * @var \yii2kernel\web\View                  $this
 * @var \yiitrix\models\record\Block          $blockModel
 * @var \yiitrix\models\record\Section        $sectionModel
 * @var \yii\data\ActiveDataProvider          $dataProvider
 * @var \yiitrix\modules\admin\search\Section $searchModel
 * @var \yiitrix\modules\admin\forms\Section  $formModel
 */

declare(strict_types=1);

use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;
use yii2kernel\grid\ActionColumn;
use yiitrix\modules\admin\search\Section;

$this->title = \Yii::t('yiitrix', 'Sections of block "{code}"', ['code' => $blockModel->code]);

$this->breadcrumbs[] = [
    'label' => \Yii::t('yiitrix', 'Blocks'),
    'url'   => ['index'],
];

$this->breadcrumbs[] = [
    'label' => \Yii::t('yiitrix', 'Block "{code}"', ['code' => $blockModel->code]),
    'url'   => ['form', 'block' => $blockModel->code],
];

if ($sectionModel !== null) {
    $this->breadcrumbs[] = [
        'label' => \Yii::t('yiitrix', 'Sections'),
        'url'   => ['sections', 'block' => $blockModel->code],
    ];

    $section = $sectionModel;

    $sections = [
        $section->name,
    ];

    while ($section = $section->parentSection) {
        $sections[] = [
            'label' => $section->name,
            'url'   => ['sections', 'block' => $blockModel->code, 'section' => $section->code],
        ];
    }

    foreach (array_reverse($sections) as $item) {
        $this->breadcrumbs[] = $item;
    }
} else {
    $this->breadcrumbs[] = \Yii::t('yiitrix', 'Sections');
}

?>

<section class="section-block-sections">
    <p>
        <?= Html::a('<i class="fa fa-plus"></i> ' . \Yii::t('yiitrix', 'Create'), [
            'form-section',
            'block'        => $blockModel->code,
            'section_code' => $sectionModel->code ?? null,
        ], [
            'class' => 'btn btn-success',
        ]); ?>
    </p>

    <?php Pjax::begin(['id' => 'block-section-grid-pjax', 'submitEvent' => 'submit change']); ?>

    <?= Html::beginForm([
        'sections',
        'block'                        => $blockModel->code,
        $dataProvider->sort->sortParam => Yii::$app->getRequest()->getQueryParam($dataProvider->sort->sortParam),
    ], 'get', [
        'id'        => 'block-section-grid-form',
        'data-pjax' => true,
    ]); ?>

    <?= GridView::widget([
        'id'           => 'block-section-grid',
        'dataProvider' => $dataProvider,
        'layout'       => '{summary}<div class="table-responsive">{items}</div>{pager}',
        'columns'      => [
            [
                'attribute' => 'code',
                'header'    => Html::activeTextInput($searchModel, 'code', [
                        'placeholder' => $searchModel->getAttributeLabel('code'),
                    ]) . $dataProvider->sort->link('code'),
            ],
            [
                'attribute' => 'section_code',
                'header'    => Html::activeTextInput($searchModel, 'section_code', [
                        'placeholder' => $searchModel->getAttributeLabel('section_code'),
                    ]) . $dataProvider->sort->link('section_code'),
            ],
            [
                'attribute' => 'is_active',
                'format'    => 'boolean',
                'header'    => Html::activeDropDownList($searchModel, 'is_active', Html::boolLabels(), [
                        'prompt' => $searchModel->getAttributeLabel('is_active'),
                    ]) . $dataProvider->sort->link('is_active'),
            ],
            [
                'attribute' => 'name',
                'header'    => Html::activeTextInput($searchModel, 'name', [
                        'placeholder' => $searchModel->getAttributeLabel('name'),
                    ]) . $dataProvider->sort->link('name'),
            ],
            [
                'attribute' => 'sort',
                'header'    => Html::activeInput('number', $searchModel, 'sort', [
                        'placeholder' => $searchModel->getAttributeLabel('sort'),
                    ]) . $dataProvider->sort->link('sort'),
            ],
            [
                'attribute' => 'created_at',
                'format'    => 'datetime',
                'header'    => Html::textInput('created_at', null, [
                        'placeholder' => $searchModel->getAttributeLabel('created_at'),
                        'disabled'    => true,
                    ]) . $dataProvider->sort->link('created_at'),
            ],
            [
                'attribute' => 'updated_at',
                'format'    => 'datetime',
                'header'    => Html::textInput('updated_at', null, [
                        'placeholder' => $searchModel->getAttributeLabel('updated_at'),
                        'disabled'    => true,
                    ]) . $dataProvider->sort->link('updated_at'),
            ],
            [
                'class'    => ActionColumn::class,
                'template' => '<div class="btn-group">{sections}{form}{delete}</div>',
                'buttons'  => [
                    'sections' => function ($url, Section $model, $key) {
                        return Html::a('<i class="fa fa-list"></i>', ['sections', 'block' => $model->block_code, 'section' => $model->code], [
                            'title'      => Yii::t('yiitrix', 'Block sections'),
                            'aria-label' => Yii::t('yiitrix', 'Block sections'),
                            'class'      => 'btn btn-primary',
                            'data-pjax'  => '0',
                        ]);
                    },
                    'form'     => function ($url, Section $model, $key) {
                        return Html::a('<i class="fa fa-pencil"></i>', ['form-section', 'block' => $model->block_code, 'section' => $model->code], [
                            'title'      => Yii::t('yii', 'Update'),
                            'aria-label' => Yii::t('yii', 'Update'),
                            'class'      => 'btn btn-primary',
                            'data-pjax'  => '0',
                        ]);
                    },
                    'delete'   => function ($url, Section $model, $key) {
                        return Html::a('<i class="fa fa-times"></i>', ['delete', 'block' => $model->block_code, 'section' => $model->code], [
                            'title'        => Yii::t('yii', 'Delete'),
                            'aria-label'   => Yii::t('yii', 'Delete'),
                            'class'        => 'btn btn-danger',
                            'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                            'data-method'  => 'post',
                            'data-pjax'    => '0',
                        ]);
                    },
                ],
                'options'  => [
                    'width' => 120,
                ],
            ],
        ],
    ]); ?>

    <?= Html::endForm(); ?>

    <?php Pjax::end(); ?>
</section>
