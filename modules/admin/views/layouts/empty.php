<?php

/**
 * @var \yii2kernel\web\View $this
 * @var string               $content
 */

declare(strict_types=1);

use yii\helpers\Html;
use yiitrix\modules\admin\assets\LayoutAsset;

LayoutAsset::register($this);

\yii2kernel\widgets\Message::widget();
?>
<?php $this->beginPage(); ?>
<!DOCTYPE html>
<html lang="<?= \Yii::$app->language; ?>">
<head>
    <meta charset="<?= \Yii::$app->charset; ?>">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <?= Html::csrfMetaTags(); ?>

    <?php $this->head(); ?>

    <title><?= Html::encode($this->title ? : \Yii::$app->name); ?></title>
</head>
<body>
<?php $this->beginBody(); ?>

<div class="page-wrapper">
    <div class="container">
        <?php if (!empty($this->title)): ?>
            <h1><?= Html::encode($this->title); ?></h1>
        <?php endif; ?>

        <?= $content; ?>
    </div>
</div>

<?php $this->endBody(); ?>
</body>
</html>
<?php $this->endPage(); ?>
