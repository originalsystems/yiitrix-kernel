<?php

/**
 * @var \yii2kernel\web\View $this
 * @var string               $content
 */

declare(strict_types=1);

use yii\helpers\Html;

$asset = \yiitrix\modules\admin\assets\LayoutAsset::register($this);
?>
<?php $this->beginPage(); ?>
<!doctype html>
<html lang="<?= \Yii::$app->language; ?>">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?= Html::csrfMetaTags(); ?>
    <title><?= Html::encode($this->title ? : \Yii::$app->name); ?></title>

    <?php $this->head(); ?>
</head>
<body>
<?php $this->beginBody(); ?>

<header>
    <nav>
        <button type="button" class="menu-toggle" data-toggle="menu">
            <span class="bar"></span>
            <span class="bar"></span>
            <span class="bar"></span>
        </button>

        <a href="/" class="logo">
            <img src="<?= $asset->baseUrl; ?>/img/logo-horizontal.png" alt="">
        </a>

        <?php if (count(\Yii::$app->languages) > 1): ?>
            <ul class="language-switch">
                <?php foreach (\Yii::$app->languages as $language): ?>
                    <li class="<?= \Yii::$app->language === $language ? 'active' : ''; ?>">
                        <?= Html::a("<img src=\"{$asset->baseUrl}/img/flags/{$language}.png\">", [null, '_lang' => $language] + $_GET); ?>
                    </li>
                <?php endforeach; ?>
            </ul>
        <?php endif; ?>

        <?= \yii\bootstrap\Nav::widget([
            'dropDownCaret' => false,
            'encodeLabels'  => false,
            'items'         => [
                [
                    'url'     => '#',
                    'label'   => '<span class="avatar"><img src="' . (\Yii::$user->image->thumbUrl60 ?? null) . '" onerror="this.src=\'' . $asset->baseUrl . '/img/no-avatar.png\';" alt=""></span><span class="name">' . Html::encode(\Yii::$user->login ?? null) . '</span>',
                    'active'  => false,
                    'options' => [
                        'class' => 'profile-link',
                    ],
                    'items'   => [
                        [
                            'url'         => ['/admin/default/index'],
                            'label'       => \Yii::t('yiitrix', 'Cabinet dashboard'),
                            'linkOptions' => ['class' => 'fa-line-chart'],
                        ],
                        [
                            'url'         => ['/admin/user/index'],
                            'label'       => \Yii::t('yiitrix', 'Users'),
                            'active'      => \Yii::$app->controller->id === 'user',
                            'linkOptions' => ['class' => 'fa-users'],
                        ],
                        [
                            'url'         => ['/admin/block/index'],
                            'label'       => \Yii::t('yiitrix', 'Blocks'),
                            'active'      => \Yii::$app->controller->id === 'block',
                            'linkOptions' => ['class' => 'fa-cubes'],
                        ],
                        [
                            'label'   => null,
                            'options' => ['class' => 'divider'],
                        ],
                        [
                            'url'         => ['/admin/auth/sign-out'],
                            'label'       => \Yii::t('yiitrix', 'Sign out'),
                            'linkOptions' => ['class' => 'fa-sign-out'],
                        ],
                    ],
                ],
            ],
            'options'       => [
                'class' => [
                    'widget' => 'navigation',
                ],
            ],
        ]); ?>
    </nav>
</header>

<div class="page-wrapper">
    <aside>
        <?= \yii\widgets\Menu::widget([
            'activateParents' => true,
            'encodeLabels'    => false,
            'submenuTemplate' => '<ul class="dropdown-menu">{items}</ul>',
            'items'           => [
                [
                    'url'      => ['/admin/default/index'],
                    'label'    => \Yii::t('yiitrix', 'Cabinet dashboard'),
                    'active'   => \Yii::$app->controller->id === 'default',
                    'template' => '<a href="{url}" class="fa-line-chart">{label}</a>',
                ],
                [
                    'url'      => ['/admin/user/index'],
                    'label'    => \Yii::t('yiitrix', 'Users'),
                    'active'   => \Yii::$app->controller->id === 'user',
                    'template' => '<a href="{url}" class="fa-users">{label}</a>',
                ],
                [
                    'url'      => ['/admin/block/index'],
                    'label'    => \Yii::t('yiitrix', 'Blocks'),
                    'active'   => \Yii::$app->controller->id === 'block',
                    'template' => '<a href="{url}" class="fa-cubes">{label}</a>',
                ],
                [
                    'url'      => ['/admin/auth/sign-out'],
                    'label'    => \Yii::t('yiitrix', 'Sign out'),
                    'template' => '<a href="{url}" class="fa-sign-out">{label}</a>',
                ],
            ],
            'options'         => [
                'class' => [
                    'widget' => 'navigation-menu',
                ],
            ],
        ]); ?>
    </aside>

    <main>
        <div class="container-fluid">
            <?php if (count($this->breadcrumbs) > 0): ?>
                <nav aria-label="breadcrumb">
                    <?= \yii\widgets\Breadcrumbs::widget([
                        'links'              => $this->breadcrumbs,
                        'itemTemplate'       => '<li class="breadcrumb-item">{link}</li>',
                        'activeItemTemplate' => '<li class="breadcrumb-item" aria-current="page">{link}</li>',
                    ]); ?>
                </nav>
            <?php endif; ?>

            <?php if (!empty($this->title)): ?>
                <h1><span><?= Html::encode($this->title); ?></span></h1>
            <?php endif; ?>

            <?= $content; ?>
        </div>
    </main>
</div>

<?php $this->endBody(); ?>
</body>
</html>

<?php $this->endPage(); ?>
