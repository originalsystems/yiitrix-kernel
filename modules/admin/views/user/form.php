<?php

/**
 * @var \yii2kernel\web\View              $this
 * @var \yiitrix\models\User              $model
 * @var \yiitrix\modules\admin\forms\User $formModel
 */

declare(strict_types=1);

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yiitrix\models\User;
use yiitrix\rbac\AuthManager;

$this->title = $model->getIsNewRecord() ? \Yii::t('yiitrix', 'Create new user') : \Yii::t('yiitrix', 'Update user "{login}"', ['login' => $formModel->login]);

$this->breadcrumbs[] = ['label' => \Yii::t('yiitrix', 'Users'), 'url' => ['index']];
$this->breadcrumbs[] = $this->title;
?>
<section class="section-user-form">
    <?php $form = ActiveForm::begin([
        'enableAjaxValidation'   => true,
        'enableClientValidation' => false,
        'fieldConfig'            => [
            'template'     => '{label}<div class="col-sm-9">{input}{error}</div>',
            'labelOptions' => [
                'class' => 'control-label col-sm-3',
            ],
        ],
        'options'                => [
            'enctype' => 'multipart/form-data',
            'class'   => 'form-horizontal',
        ],
    ]); ?>

    <div class="img-wrapper">
        <?= Html::tag('label', '', [
            'for'             => Html::getInputId($formModel, 'image'),
            'class'           => 'img',
            'style'           => $model->image !== null ? "background-image: url({$model->image->thumbUrl360});" : null,
            'data-background' => $model->image !== null ? $model->image->thumbUrl360 : null,
        ]); ?>

        <div class="controls">
            <label class="btn btn-primary btn-block btn-change-image">
                <?= Html::activeFileInput($formModel, 'image', [
                    'id'     => Html::getInputId($formModel, 'image'),
                    'accept' => 'image/*',
                ]); ?>

                <?= \Yii::t('yiitrix', 'Choose profile image') ?>
            </label>

            <label class="btn btn-secondary btn-block btn-delete-image">
                <?= Html::activeCheckbox($formModel, 'delete_image', [
                    'label'     => false,
                    'data-role' => 'delete-image',
                ]); ?>

                <?= Yii::t('yiitrix', 'Delete image'); ?>
            </label>
        </div>
    </div>

    <?= $form->field($formModel, 'login')->textInput([
        'maxlength'    => true,
        'autocomplete' => 'off',
    ]); ?>

    <?= $form->field($formModel, 'status')->dropDownList(User::statusLabels(), [
        'autocomplete' => 'off',
        'prompt'       => \Yii::t('yiitrix', 'Specify one of values'),
    ]); ?>

    <?= $form->field($formModel, 'email')->textInput([
        'maxlength'    => true,
        'autocomplete' => 'off',
    ]); ?>

    <?= $form->field($formModel, 'roles')->dropDownList(AuthManager::roleLabels(), [
        'multiple' => true,
        'prompt'   => \Yii::t('yiitrix', 'Choose user roles'),
    ]); ?>

    <?= $form->field($formModel, 'password')->passwordInput([
        'maxlength'    => true,
        'autocomplete' => 'off',
    ]); ?>

    <?= $form->field($formModel, 'password_repeat')->passwordInput([
        'maxlength'    => true,
        'autocomplete' => 'off',
    ]); ?>

    <div class="form-group">
        <div class="col-sm-9 col-sm-offset-3">
            <button type="submit" class="btn btn-primary"><?= Yii::t('yiitrix', 'Submit'); ?></button>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</section>
