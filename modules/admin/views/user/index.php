<?php

/**
 * @var \yii2kernel\web\View               $this
 * @var \yii\data\ActiveDataProvider       $dataProvider
 * @var \yiitrix\modules\admin\search\User $searchModel
 */

declare(strict_types=1);

use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;
use yii2kernel\grid\ActionColumn;

$this->title = \Yii::t('yiitrix', 'Users');

$this->breadcrumbs[] = $this->title;
?>
<section class="section-user-index">
    <p>
        <?= Html::a('<i class="fa fa-plus"></i> ' . \Yii::t('yiitrix', 'Create'), ['form'], ['class' => 'btn btn-success']); ?>
    </p>

    <?php Pjax::begin(); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'layout'       => '{summary}<div class="table-responsive">{items}</div>{pager}',
        'columns'      => [
            'id',
            [
                'attribute' => 'status',
                'value'     => 'statusLabel',
                'filter'    => \yiitrix\models\User::statusLabels(),
            ],
            'email',
            'login',
            'updated_at:datetime',
            [
                'class'    => ActionColumn::class,
                'template' => '<div class="btn-group btn-group-sm">{form}{delete}</div>',
                'buttons'  => [
                    'form'   => function ($url, $model, $key) {
                        return Html::a('<i class="fa fa-pencil"></i>', $url, [
                            'title'      => Yii::t('yii', 'Update'),
                            'aria-label' => Yii::t('yii', 'Update'),
                            'class'      => 'btn btn-secondary',
                            'data-pjax'  => '0',
                        ]);
                    },
                    'delete' => function ($url, $model, $key) {
                        return Html::a('<i class="fa fa-times"></i>', $url, [
                            'title'        => Yii::t('yii', 'Delete'),
                            'aria-label'   => Yii::t('yii', 'Delete'),
                            'class'        => 'btn btn-danger',
                            'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                            'data-method'  => 'post',
                            'data-pjax'    => '0',
                        ]);
                    },
                ],
                'options'  => [
                    'width' => 80,
                ],
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</section>
