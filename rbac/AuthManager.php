<?php

declare(strict_types=1);

namespace yiitrix\rbac;

use yii\rbac\DbManager;

class AuthManager extends DbManager
{
    public const ENTER_FRONTEND = 'enter-frontend';
    public const ENTER_BACKEND  = 'enter-backend';

    public const ROLE_ADMIN  = 'admin';
    public const ROLE_CLIENT = 'client';

    public $itemTable       = '"auth"."item"';
    public $itemChildTable  = '"auth"."item_child"';
    public $assignmentTable = '"auth"."assignment"';
    public $ruleTable       = '"auth"."rule"';

    public static function roleLabels()
    {
        return [
            self::ROLE_ADMIN  => \Yii::t('yiitrix', 'Administrator'),
            self::ROLE_CLIENT => \Yii::t('yiitrix', 'Client'),
        ];
    }
}
