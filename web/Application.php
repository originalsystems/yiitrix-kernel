<?php

declare(strict_types=1);

namespace yiitrix\web;

use yii\base\Action;
use yii\base\InvalidConfigException;

class Application extends \yii\web\Application
{
    public  $languages = [];
    private $defaultLanguage;

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function init(): void
    {
        $this->defaultLanguage = $this->language;

        if (in_array($this->language, $this->languages, true) === false) {
            throw new InvalidConfigException('Default language should be is an available languages');
        }

        parent::init();
    }

    /**
     * @param \yii\base\Action $action
     *
     * @return bool
     */
    public function beforeAction($action): bool
    {
        $this->languageSettingUp($action);

        return parent::beforeAction($action);
    }

    private function languageSettingUp(Action $action): void
    {
        if ($action->id === 'error') {
            $this->language = $this->defaultLanguage;

            return;
        }

        $language = $this->getRequest()->get('_lang');

        if ($language === null) {
            return;
        }

        if ($language === $this->defaultLanguage || in_array($language, $this->languages, true) === false) {
            $params = $this->getRequest()->getQueryParams();

            unset($params['_lang']);

            $this->getResponse()->redirect([null] + $params);
            $this->end();
        }

        $this->language = $language;
    }
}
