<?php

declare(strict_types=1);

namespace yiitrix\web;

use yii\base\BootstrapInterface;

class User extends \yii\web\User implements BootstrapInterface
{
    public function bootstrap($app)
    {
        if (property_exists('Yii', 'user')) {
            \Yii::$user = $this->getIdentity();
        }
    }
}
