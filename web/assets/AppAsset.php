<?php

declare(strict_types=1);

namespace yiitrix\web\assets;

use yii2kernel\web\AssetBundle;

class AppAsset extends AssetBundle
{
    public $sourcePath = '@yiitrix/web/assets/app';
    public $js         = [
        'app.js',
    ];
}
