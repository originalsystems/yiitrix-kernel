window.App = (function() {
    'use strict';

    let socket = null;

    return {
        search_values:           function(only, except) {
            let values = [];

            if (location.search !== '') {
                values = decodeURIComponent(location.search.substr(1)).split('&').map(function(item) {
                    let name = item.split('=')[0];

                    if (Array.isArray(only) && only.indexOf(name) === -1) {
                        return null;
                    }

                    if (Array.isArray(except) && except.indexOf(name) !== -1) {
                        return null;
                    }

                    return {
                        name:  name,
                        value: item.split('=')[1]
                    };
                }).filter(function(item) {return !!item;});
            }

            return values;
        },
        /**
         * @param {string|Element|jQuery} form
         * @return {Object}
         */
        search_values_with_form: function(form) {
            let $form  = jQuery(form),
                names  = [],
                query  = {},
                values = $form.serializeArray();

            $form.find('[name]').each(function() {
                let $this = jQuery(this);

                names.push($this.attr('name'));
            });

            values = values.concat(this.search_values(null, names));

            values.forEach(function(item) {
                if (/\[\d*\]$/.test(item.name)) {
                    let name = item.name.replace(/\[\d*\]$/, '');

                    if (Array.isArray(query[name]) === false) {
                        query[name] = [];
                    }

                    if (query[name].indexOf(item.value) === -1) {
                        query[name].push(item.value);
                    }
                } else {
                    query[item.name] = item.value;
                }
            });

            Object.keys(query).forEach(function(key) {
                if (query[key] === '') {
                    delete query[key];
                }
            });

            return query;
        },

        number_format: function(number) {
            let string = number + '',
                out    = '';

            string.split('').reverse().forEach(function(num, i) {
                if (i !== 0 && i % 3 === 0) {
                    out = ' ' + out;
                }

                out = num + out;
            });

            return out;
        },

        meta: function(name) {
            let $meta   = document.querySelector('meta[content][name="' + name + '"]'),
                content = null;

            if ($meta !== null) {
                let attribute = $meta.getAttribute('content');

                try {
                    content = JSON.parse(attribute);
                } catch (error) {
                    content = attribute;
                }
            }

            return content;
        },

        post: function(url, data) {
            return new Promise(function(resolve, reject) {
                jQuery.ajax({
                    type:     'POST',
                    url:      url,
                    data:     data,
                    dataType: 'json',
                    success:  function(json) {
                        if (json.success !== true) {
                            reject(json.errors);

                            return;
                        }

                        resolve(json.data);
                    },
                    error:    function() {
                        reject(['Unexpected error']);
                    }
                });
            });
        },

        wait: function(title) {
            let $body = jQuery('body');

            if (jQuery.fn.waitMe) {
                $body.waitMe({
                    effect: 'bounce',
                    text:   title,
                    bg:     'rgba(0,0,0,0.75)',
                    color:  'white'
                });
            }
        },

        resume: function() {
            let $body = jQuery('body');

            if (jQuery.fn.waitMe) {
                $body.waitMe('hide');
            }
        },

        popup: function(url) {
            return new Promise(function(resolve, reject) {
                let width   = 1200,
                    height  = 600,
                    left    = (window.screen.width - width) / 2,
                    top     = (window.screen.height - height) / 2,
                    options = 'resizable=yes,scrollbars=no,toolbar=no,menubar=no,location=no,directories=no,status=yes';

                options += ',width=' + width;
                options += ',height=' + height;
                options += ',left=' + left;
                options += ',top=' + top;

                try {
                    let popup  = window.open('about:blank', 'app.popup', options),
                        script = popup.document.createElement('script'),
                        interval;

                    script.attributes.type = 'text/javascript';
                    script.textContent     = 'location.href = "' + url + '";';

                    popup.document.body.appendChild(script);

                    App.wait('Закройте всплывающее окно');

                    interval = setInterval(function() {
                        if (popup.closed) {
                            popup = null;
                            clearInterval(interval);

                            App.resume();

                            resolve();
                        }
                    }, 250);
                } catch (err) {
                    reject(err);
                }
            });
        },

        socket: function() {
            let config = this.meta('socket-io');

            if (!config) {
                return null;
            }

            if (socket === null) {
                socket = io.connect(config.url);

                socket.emit('user-login', config.user);

                socket.on('reconnect_attempt', () => {
                    socket.emit('user-login', config.user);
                });
            }

            return socket;
        }
    };
})();
