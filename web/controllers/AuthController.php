<?php

declare(strict_types=1);

namespace yiitrix\web\controllers;

use yii2kernel\web\Controller;
use yiitrix\web\forms\Login;

class AuthController extends Controller
{
    public $loginView = 'login';

    /**
     * @return string|\yii\web\Response
     * @throws \yii\base\InvalidParamException
     */
    public function actionLogin()
    {
        $formModel = new Login();

        if ($formModel->load(\Yii::$app->getRequest()->post())) {
            $formModel->save();

            if (\Yii::$app->getRequest()->getIsAjax()) {
                return $this->json([], $formModel->getErrors());
            }

            if ($formModel->hasErrors() === false) {
                return $this->goBack();
            }
        }

        return $this->render($this->loginView, [
            'formModel' => $formModel,
        ]);
    }

    public function actionLogout()
    {
        \Yii::$app->getUser()->logout();

        if (\Yii::$app->getRequest()->getIsAjax()) {
            return $this->json();
        }

        return $this->goHome();
    }
}
